<?php
session_start();

if(isset($_POST['action']) && $_POST['action']=='leave'){
	
	require("db/Users.php");	 		  
	$objUser= new Users();
	$objUser->setLoginstatus(0);
	   $objUser->setLastlogin(date('Y-m-d h:i:s'));
	     $objUser->setId($_POST['userId']);
		if($objUser->updateLoginStatus()){
			unset($_SESSION['user']);
			session_destroy();
			echo json_encode(['status'=>1, 'msg'=>'logout..']);
		}else{
			echo json_encode(['status'=>0, 'msg'=>'Something went wrong..']);
		}
}


?>