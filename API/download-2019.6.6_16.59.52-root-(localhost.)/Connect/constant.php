<?php
//for
define('STORE', 1);
define('SEND', 2);

//to
define('TO_MOBILE', 1);
define('TO_WEB', 2);

//channel
define('CH_MOBILE', 1);
define('CH_WEB', 2);

//type
define('SHOW_ONLINEUSER', 1);
define('SEND_MESSAGE', 2);
define('SEND_STATUS', 3);
define('SEND_DELIVERY', 4);
define('SEND_PROFILEDATA', 5);

//Status Code
define('MESSAGE', 1);
define('STATUS', 2);
define('DELIVERY', 3);
define('UPDATEPROFILE', 4);



/* .................. Define key constant of json object .................... */
define('FOR_MAIN_CLIENT', "for");
define('TO', "to");
define('CHANNEL', "channel");
define('TYPE', "type");

?>