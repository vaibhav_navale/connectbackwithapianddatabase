<?php
class GCM {
 
 private $conn;
    // constructor
    function __construct() {
        
		}
 
    // sending push message to single user by gcm registration id
    public function send($to, $message) {
		 //echo "1";
         $fields = array(
             
            'to' => $to,
            'data' => $message,
			
        );
		
	//	print_r ($fields);
         $this->sendPushNotification($fields);
		
    }
 
    // Sending message to a topic by topic id
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }
 
    // sending push message to multiple users by gcm registration ids
    public function sendMultiple($registration_ids, $message) {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,
        );
 
        return $this->sendPushNotification($fields);
    }
 
    // function makes curl request to gcm servers
    private function sendPushNotification($fields) {
 
        
        // include config
        include_once __DIR__ . '/../../include/config.php';
        
        
    
 
$data = json_encode($fields);


$url = 'https://fcm.googleapis.com/fcm/send';
//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key




  $server_key = 'AAAAHQZDqH8:APA91bG371IoBam1YEkwKzFix5co7xA6Y1EjSSQONfE5vY52vy2jLRpOPfhKpuWOPF2sU1TvM-0wciIKxNFmxZChb6HdoM3mdTbWAMY28zfFs1HW9-Yi1Znm0c2V8-ZaWQoZTQa7hAU0';
//header with content_type api key
$headers = array(
    'Content-Type:application/json',
    'Authorization:key='.$server_key
);
//CURL request to route notification to FCM connection server (provided by Google)
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$result = curl_exec($ch);

if ($result === FALSE) {
    
    die('Oops! FCM Send Error: ' . curl_error($ch));
    
}else{
    echo $data;
}
curl_close($ch);

return $result;
	
	}
 
}
 
?>