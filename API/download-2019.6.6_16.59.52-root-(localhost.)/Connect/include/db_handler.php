<?php
 
/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 */
class DbHandler {
 
    private $conn;
    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
    
     function getServer() {
         
   	return   "http://8b8f6cb.online-server.cloud/";
   	
    }
    
      public function getDelivery($message_id) {
        $response = array();
 
 
       
      
    // output data of each row
	  $response['error'] = false;

            // get the message
          
           /* $stmt = $this->conn->prepare("SELECT message_id,user_id,chat_room_id,local_id,message,file,created_at FROM messages WHERE message_id=?");
            $stmt->bind_param("i",$message_id);
            if ($stmt->execute()) {
				//echo ('2');
				//echo $stmt->fullQuery;
               /* $stmt->bind_result($message_id, $user_id, $chat_room_id, $local_id, $message,$file,$created_at);
                $stmt->fetch();*/
				/*$res = $stmt->get_result();
				print_r("res data");
				print_r($res);
                $row = $res->fetch_assoc();
				print_r("row data");
				print_r($row);
				$this->db->last_query();
                $tmp = array();
                $tmp['message_id'] = $row["message_id"];		
                $tmp['local_id']=$row["local_id"];                
                $tmp['message'] = $row["local_id"];
				$tmp['file']=$row["local_id"];
                $tmp['created_at'] = $row["local_id"];
                $response = $tmp;*/
				
				$sql = "SELECT * FROM messages where message_id=".$message_id;
$result = $this->conn->query($sql);

if ($result->num_rows > 0) {
   
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $tmp = array();
                $tmp['message_id'] = $row["message_id"];		
                $tmp['local_id']=$row["local_id"];                
                $tmp['message'] = $row["message"];
				$tmp['file']=$row["file"];
                $tmp['user_id'] = $row["user_id"];
                $tmp['chat_room_id'] = $row["chat_room_id"];
				//user_id,chat_room_id
                $response = $tmp;
    }
    
} else {
    echo "0 results";
}         
        
		
 
       return $response;
    }
 
    
    
    
	public function fetchrecords($contact) {
        
            $users = array();
            $query = "SELECT * FROM users WHERE contact != '" . $contact . "'" ;
           //print_r($query);
           // $stmt = $this->conn->prepare("SELECT id, username, contact,password,gcm_registration_id,created_at FROM users1 WHERE contact != " . $contact);
            $stmt = $this->conn->prepare($query);
			$stmt->execute();
            $result = $stmt->get_result();
	       //echo $this-db->last_query();
            while ($user = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $user['user_id'];
                $tmp["username"] = $user['name'];
                $tmp["contact"] = $user['contact'];
				$tmp["password"] = $user['password'];
                $tmp["gcm_registration_id"] = $user['gcm_registration_id'];
                $tmp["created_at"] = $user['created_at'];				
				 $tmp["image"]=$user['image'];	
				  $tmp["about"]=$user['about'];	
			     
                array_push($users, $tmp);
            }
			return $users;
        }
		
  
  
  public function fetchContactDetails($contact) {
        
           // $users = array();
            $query = "SELECT * FROM users WHERE contact = '" . $contact . "'" ;
           //print_r($query);
           // $stmt = $this->conn->prepare("SELECT id, username, contact,password,gcm_registration_id,created_at FROM users1 WHERE contact != " . $contact);
            $stmt = $this->conn->prepare($query);
			$stmt->execute();
            $result = $stmt->get_result();
	       //echo $this-db->last_query();
            while ($user = $result->fetch_assoc()) {
                //$tmp = array();
                $tmp["id"] = $user['user_id'];
                $tmp["username"] = $user['name'];
                $tmp["contact"] = $user['contact'];
				$tmp["password"] = $user['password'];
                $tmp["gcm_registration_id"] = $user['gcm_registration_id'];
                $tmp["created_at"] = $user['created_at'];				
				 $tmp["image"]=$user['image'];	
				  $tmp["about"]=$user['about'];	
			     
               // array_push($users, $tmp);
            }
			return $tmp;
        }
  
    public function UserStatusUpdateFalse($id,$status,$contact){
       
	   $response;
	   
         $user_id = (int)$id;
       
        // First check if user already existed in db
        if ($this->isContactExistsInUserStatus($contact)) {
		
           // User with same contact already existed in the db

         
                  $stmt = $this->conn->prepare("UPDATE user_status SET status = ? WHERE user_id=?");
			 
                  $stmt->bind_param("ss",$status,$user_id);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
                  {
                      $response="SuccessStatusUpdate";
			  
                  } 
                  else 
			     {
                      // Failed to create user
                       $response="updatefailedindatabaseforupdatestatus";
                }
        }else{
          echo "Contact not Exits";
        }
		
       return $response;
	//  echoRespnse(200,$response);
    }
  
  
  public function UserStatusUpdateTimely($id,$time,$contact){
    
     $response;
    
         $user_id = (int)$id;
       
        // First check if user already existed in db
        if ($this->isContactExistsInUserStatus($contact)) {
		
           // User with same contact already existed in the db

         
                  $stmt = $this->conn->prepare("UPDATE user_status SET time = ? WHERE user_id=?");
			 
                  $stmt->bind_param("ss",$time,$user_id);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
                  {
                      $response="SuccessUpdate";
			  
                  } 
                  else 
			     {
                      // Failed to create user
                       $response="updatefailedindatabase";
                }
        }else{
          echo "Contact not Exits";
        }
		
       return $response;
	//  echoRespnse(200,$response);
    }
  
  
  public function UserStatus($id,$name,$contact,$status,$epoch) {
	  
	  $response;
         $user_id = (int)$id;
        // First check if user already existed in db
		
		$result=$this->isContactExistsInUserStatus($contact);
        if ($result>=1) {
		 // User with same contact already existed in the db
	    	  
         
                  $stmt = $this->conn->prepare("UPDATE user_status SET  user_id=?, name=?, status = ?, time = ? WHERE contact=?");
			 
                  $stmt->bind_param("issss",$user_id,$name,$status,$epoch,$contact);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
                  {
                      $response="SuccessUpdate";
			  
                  } 
                  else 
			     {
                      // Failed to create user
                       $response="updatefailedindatabase";
                }
          
        }
		else 
		{
			
			  // insert query
			//echo "hello";
            $stmt = $this->conn->prepare("INSERT INTO user_status(user_id,name,contact,status,time) values(?,?,?,?,?)");
            $stmt->bind_param("issss", $user_id, $name,$contact,$status,$epoch);
 
            $result = $stmt->execute();
         //  print_r($stmt);
            $stmt->close();
             
            // Check for successful insertion
            if ($result) 
			{
               $response="Successinsert";

            } 
			else 
			{
               // Failed to create user
                   $response="insertfailedindatabase";
            }
			
			
        }
 
       return $response;
	//  echoRespnse(200,$response);
    }
  
  
  
  public function UserStatusFromWebSocket($id,$status,$epoch) {  
	  
    	  $response;
         $user_id = (int)$id;

                   $statuspass="";
		             if($status){
					    $statuspass=1;	 
					 }else{
						 $statuspass=0;
					 }
					         
                  $stmt = $this->conn->prepare("UPDATE user_status SET  status = ?, time = ? WHERE user_id=?");
			 
                  $stmt->bind_param("ssi",$statuspass,$epoch,$user_id);

                  $result = $stmt->execute();
               
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
				  {
                      $response="SuccessUpdate";			  
                  } 
                  else 
			     {
                      // Failed to create user
                       $response="updatefailedindatabase";
                }          
      
		
       return $response;
	//  echoRespnse(200,$response);
    }
  
  
		public function RegisterUser($username,$contact,$password,$file_url,$about,$id,$epoch) {
        
         $response;
        // First check if user already existed in db
        if ($this->isContactExists($contact)) {
			

            // insert query
			
			$otp=null;
            $stmt = $this->conn->prepare("UPDATE  users SET name=?, password=?, image=?, about=?, created_at=?, otp=? where contact=?");
            $stmt->bind_param("sssssss", $username,$password,$file_url,$about,$epoch,$otp,$contact);
 
            $result = $stmt->execute();
			
         //  print_r($stmt);
            $stmt->close();
             
            // Check for successful insertion
            if ($result) 
			{
             
               $response="Success";
			 $this->chatrooms_insert($contact,$epoch);
			
			 $status="0";
			 $this->UserStatus($id,$username,$contact,$status,$epoch);
			
			 
			  
            } 
			else 
			{
               // Failed to create user
               $response="Failedtocreate";
            }
        }
		else 
		{
            // User with same contact already existed in the db
		//	echo "hi";
           $response="Numberisnotverified";
        }
 
       return $response;
	//  echoRespnse(200,$response);
    }
	
	
	
	
	
	//-------------------------Update User data---------------------------------------------//

		public function updateData($user_id,$name,$file_url,$about) {
        
              $response;
			//$stmt = $this->conn->prepare("UPDATE users SET name = ?,image = ?, about = ? WHERE user_id='" . $user_id . "'");
			 $stmt = $this->conn->prepare("UPDATE users SET  name = ?,image = ?, about = ? WHERE user_id=?");
			 
            $stmt->bind_param("sssi",$name,$file_url,$about,$user_id);
 
            $result = $stmt->execute();
           //print_r($stmt);
            $stmt->close();
             
            // Check for successful insertion
            if ($result) 
			{              
		       $response['image'] = $file_url;
			   $response['about']=$about;
			   $response['name']=$name;               
            } 
			else 
			{
               // Failed to create user
               $response="FailtoUpdate";
            }
      // return $response;
	 // echoRespnse(200,$response);
    }
	
	//-------------------------delete message start---------------------------------------------//
	    // delete message
    public function deleteMessage($user_id, $chatroom_id) {
        $response = array();
        $stmt = $this->conn->prepare("delete from 	messages where user_id=? and chat_room_id=?");
        $stmt->bind_param("ii", $user_id, $chatroom_id);
 
        if ($stmt->execute()) {
            // User successfully updated
            $response["error"] = false;
            $response["message"] = 'Success';
        } else {
            // Failed to update user
            $response["error"] = true;
            $response["message"] = "Fail";
            $stmt->error;
        }
        $stmt->close();
 
        return $response;
    }
	
	//-------------------------------delete message end-----------------------------------------------//
	
	
	//-------------------------delete file start---------------------------------------------//
	    // delete message
    public function deleteFile($file_path) {
		
        if(unlink($file_path))
		{	
			 $response["error"] = false;
             $response["message"] = 'Success';
			 
		}else{
			     $response["error"] = true;
                 $response["message"] = "Fail";
		}
        return $response;
    }
	
	//-------------------------------delete file end-----------------------------------------------//
	
	
	
public function  chatrooms_insert($contact,$epoch)
{
	 $users = array();

            $query = "SELECT user_id,name FROM users WHERE contact='" . $contact . "'" ;
          
            $stmt = $this->conn->prepare($query);

            $stmt->execute();

            $result = $stmt->get_result();
        
            while ($user = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $user['user_id'];
                $tmp["name"] = $user['name'];
             array_push($users, $tmp);
            }



            $result=$this->isContactExistsInChatRoom($tmp["id"]);

            if ($result>=1) {

         
                  $stmt = $this->conn->prepare("UPDATE chat_rooms SET   name=?, created_at = ? WHERE   chat_room_id=?");
       
                  $stmt->bind_param("ssi",$tmp['name'],$epoch,$tmp["id"]);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
                  {
                      $response="SuccessUpdate";
        
                  } 
                  else 
                 {
                      // Failed to create user
                       $response="updatefailedindatabase";
                 }

            }else{


     //echo $tmp['id'];
                $stmt = $this->conn->prepare("Insert into chat_rooms(chat_room_id,name,created_at) values(?,?,?)");

                $stmt->bind_param("sss", $tmp['id'], $tmp['name'],$epoch);
     
                $result = $stmt->execute();
               // echoRespnse(200, $result);
                $stmt->close();


            }


}
	
	
	private function isContactExists($contact) {
        $stmt = $this->conn->prepare("SELECT user_id from users WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    } 
	
  
  private function isContactExistsInUserStatus($contact) {
    
    
        $stmt = $this->conn->prepare("SELECT user_id from user_status WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    } 

     private function isContactExistsInChatRoom($chat_room_id) {
    
    
        $stmt = $this->conn->prepare("SELECT name from chat_rooms WHERE chat_room_id = ?");
        $stmt->bind_param("i", $chat_room_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    } 
	
    /**
     * Checking for duplicate user by contact address
     * @param String $contact contact to check in db
     * @return boolean
     */
    private function isUserExists($contact,$password) {
        $stmt = $this->conn->prepare("SELECT user_id from users WHERE contact = ? and password= ?");
        $stmt->bind_param("ss", $contact,$password);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
 
 
  // creating new user if not existed
    public function ConfirmOTP($userEnterOtp,$contact) {
        $response = array();
 
       $response = $this->getUserByContactConfirmOTP($contact);
   
      if($response["otp"]==$userEnterOtp){
	      $response["success"] =true;
	  }else{
	     $response["success"] =false;
	  }
 
        return $response;
    }
		
    // creating new user if not existed
    public function createUser($contact, $password) {
        $response = array();
 
        // First check if user already existed in db
        if ($this->isUserExists($contact,$password)) {           
      

            // User with same contact already existed in the db
            $response["error"] = false;
			$response["success"]=true;
            $response["user"] = $this->getUserByContact($contact);
        }
		else
		{
			 $response["error"] = false;
			$response["success"]=false;
            $response["message"] = 'contact or password is not valid';
            // echoRespnse(400, $response);
		}
 
        return $response;
    }
	
	//Update password
	public function resetPassword($contact,$confirmpassword){
		
		          $response = array();
	
	              $stmt = $this->conn->prepare("UPDATE users SET password = ? WHERE contact=?");
			 
                  $stmt->bind_param("ss",$confirmpassword,$contact);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
                  {
                      $response[]="password Update"; 
					  $response["success"]=true;
                  } 
                  else 
			      {
                      // Failed to create user
                      $response[]="password not Update";
					  $response["success"]=false;
					 
                  }
				  
	
	
        return $response;
		  
	}
	
	//set null otp 
	public function SetNullOTP($contact) {
		$response = array();
	   if ($this->isContactExists($contact)) {
		 
		 			     $otp = null;
			 
			 
			      $stmt = $this->conn->prepare("UPDATE users SET otp = ? WHERE contact=?");
			 
                  $stmt->bind_param("is",$otp,$contact);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
				  
				    if ($result) 
                  {
                      
					    $response[]="OTP Update";										
				    	$response["success"]=true;
			            $response["otp"]=$otp;
					  
                  } 
                  else 
			      {
                      // Failed to create user
                       $response[]="OTP Not Update";
                  }
		 
		 
		  }
		else
		{
			$response["success"]=false;
            $response["message"] = 'contact or password is not valid';
            // echoRespnse(400, $response);
		}
 
        return $response;
	
	
	
	}
	
	//registerWithOTP

     public function registerWithOTP($contact,$contactwithoutcontrycode) {
        $response = array();
 
 // First check if user already existed in db     
          if (!$this->isContactExists($contact)) {
			

            
			//Generating a 6 Digits OTP or verification code 
			     $otp = rand(100000, 999999);
				 
				 // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(contact,otp) values(?,?)");
            $stmt->bind_param("si", $contact ,$otp);
 
            $result = $stmt->execute();
          print_r($stmt);
            $stmt->close();
             
            // Check for successful insertion
            if ($result) 
			      {
              // $response['url'] = $file_url;


               echo $response=$this->sendOtp($otp,$contactwithoutcontrycode,$contact);
					
				    	$response["success"]=true;
			        $response["otp"]=$otp;										  
            } 
			     else 
			      {
               // Failed to create user
               $response[]="Failedtocreate";
            }
    }
		else 
		{
            // User with same contact already existed in the db
		//	echo "hi";
		   $response["success"]=false;
           $response[]="ContactAlreadyExist";
        }
 
        return $response;
    }
	
	
	//Check User Exits or Not
	 public function checkNumberExits($contact,$contactwithoutcontrycode) {
        $response = array();
 
        // First check if user already existed in db
        if ($this->isContactExists($contact)) {           

		      //   $response["user"] = $this->getUserByContact($contact);
		
			     //Generating a 6 Digits OTP or verification code 
			     $otp = rand(100000, 999999);
			 
			 
			      $stmt = $this->conn->prepare("UPDATE users SET otp = ? WHERE contact=?");
			 
                  $stmt->bind_param("is",$otp,$contact);

                  $result = $stmt->execute();
                  //  print_r($stmt);
                  $stmt->close();
             
                  // Check for successful insertion
                  if ($result) 
                  {
                      $response[]="OTP Update";
					 
					echo $response=$this->sendOtp($otp,$contactwithoutcontrycode,$contact);
					
					$response["success"]=true;
			        $response["otp"]=$otp;
					  
                  } 
                  else 
			      {
                      // Failed to create user
                       $response[]="OTP Not Update";
                  }
	  
            // User with same contact already existed in the db
			
			
            
        }
		else
		{
			$response["success"]=false;
            $response["message"] = 'contact or password is not valid';
            // echoRespnse(400, $response);
		}
 
        return $response;
    }




     public function sendOtp($otp,$contactwithoutcontrycode,$contact){

      //  $contact = $app->request->post('contact');

    $senderid="pcrllc";

   // $otp = rand(100000, 999999);



    $message="$otp is your one time password to peoceed on connect app. Do not share your OTP with anyone.";

     
      
    $authentication_key="278830Axg7OulmYGYQ5cee6037";

     

     
    echo $message." ".$contact." ".$authentication_key;



$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?otp=$otp&sender=$senderid&message=$message&mobile=$contact&authkey=$authentication_key",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_SSL_VERIFYHOST => 0,
  CURLOPT_SSL_VERIFYPEER => 0,
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
 
    

     


     }
	
	
	/*//This function will send the otp 
	 public function sendOtp($otp,$contactwithoutcontrycode,$contact){
		 
		$fields = array(
    "sender_id" => "FSTSMS",
    "message" => "$otp is your one time password to peoceed on connect app. It's valid for 1 minute. Do not share your OTP with anyone.",
    "language" => "english",
    "route" => "p",
    "numbers" => "$contactwithoutcontrycode",
);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.fast2sms.com/dev/bulk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 15,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => json_encode($fields),
  CURLOPT_HTTPHEADER => array(
    "authorization: VsaNJYbtKE5l2T6qMC7ipfLd0ocSRG3ZmWPDrnu9OQI4UygxAegfkGEH8v5yWI3szTeVlBK1Rx9SPcMp",
    "accept: *//*",
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
   $response;
}
	 
}*/
 
    // updating user GCM registration ID
    public function updateGcmID($user_id, $gcm_registration_id) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE users SET gcm_registration_id = ? WHERE user_id = ?");
        $stmt->bind_param("si", $gcm_registration_id, $user_id);
 
        if ($stmt->execute()) {
            // User successfully updated
            $response["error"] = false;
            $response["message"] = 'GCM registration ID updated successfully';
        } else {
            // Failed to update user
            $response["error"] = true;
            $response["message"] = "Failed to update GCM registration ID";
            $stmt->error;
        }
        $stmt->close();
 
        return $response;
    }
 
    // fetching single user by id
    public function getUser($user_id) {
        $stmt = $this->conn->prepare("SELECT user_id, name, contact, gcm_registration_id, created_at,image,about FROM users WHERE user_id = ?");
        $stmt->bind_param("s", $user_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($user_id, $name, $contact, $gcm_registration_id, $created_at,$image,$about);
            $stmt->fetch();
            $user = array();
			
			
            $user["user_id"] = $user_id;
            $user["name"] = $name;
            $user["contact"] = $contact;
            $user["gcm_registration_id"] = $gcm_registration_id;
            $user["created_at"] = $created_at;				
            $user["image"]=$image;		
            $user["about"]=$about;	          			
            $stmt->close();
			 $status = $this->getUserStatusOnId($user_id);
			 
			 if($status==1)
			   $user["status"]=true;
		      else
			      $user["status"]=false;
			
            return $user;
        } else {
            return NULL;
        }
    }
 
    // fetching multiple users by ids
    public function getUsers($user_ids) {
 
        $users = array();
        if (sizeof($user_ids) > 0) {
            $query = "SELECT user_id, name, contact, gcm_registration_id, created_at,image FROM users WHERE user_id IN (";
 
            foreach ($user_ids as $user_id) {
                $query .= $user_id . ',';
            }
 
            $query = substr($query, 0, strlen($query) - 1);
            $query .= ')';
 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $result = $stmt->get_result();
 
            while ($user = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["user_id"] = $user['user_id'];
                $tmp["name"] = $user['name'];
                $tmp["contact"] = $user['contact'];
                $tmp["gcm_registration_id"] = $user['gcm_registration_id'];
                $tmp["created_at"] = $user['created_at'];			
			    $tmp["image"]=$user['image'];	
		
				
                array_push($users, $tmp);
            }
        }
 
        return $users;
    }
 
    // messaging in a chat room / to persional message
    public function addMessage($user_id, $chat_room_id, $message,$file,$local_id,$epochtime) {
        $response = array();
 
       // $stmt = $this->conn->prepare("INSERT INTO users(name, contact) values(?, ?)");
        $stmt = $this->conn->prepare("INSERT INTO messages (chat_room_id, user_id, local_id, message, file,created_at) values(?, ?, ?,?,?,?)");
		 if($message==null)
		{
			$message="";
		}
		if($file==null)
		{
			$file="";
		}	
		
	
        $stmt->bind_param("iiisss", $chat_room_id, $user_id,$local_id, $message,$file,$epochtime);
		
       	
		
        
        $result = $stmt->execute();
		
		//printf("Error: %s.\n", $stmt->error);

		//return $result; 
		 
 
       if ($result) {
        

            // get the message
            $message_id = $this->conn->insert_id;
            $stmt = $this->conn->prepare("SELECT message_id, user_id, chat_room_id, local_id, message,file, created_at FROM messages WHERE message_id = ?");
            $stmt->bind_param("i", $message_id);
            if ($stmt->execute()) {
				//echo ('2');
				//echo $stmt->fullQuery;
                $stmt->bind_result($message_id, $user_id, $chat_room_id, $local_id, $message,$file,$created_at);
                $stmt->fetch();
                $tmp = array();
                $tmp['message_id'] = $message_id;		
                $tmp['local_id']=$local_id;
                $tmp['message'] = $message;
				$tmp['file']=$file;
                $tmp['created_at'] = $created_at;
                $response['message'] = $tmp;
			    $response['error'] = false;
            }
        } else {
			echo $stmt->error;
			

            $response['error'] = true;
           $response['message'] = 'Failed send message';
        }
 
       return $response;
    }
 
    // fetching all chat rooms
    public function getAllChatrooms() {
        $stmt = $this->conn->prepare("SELECT * FROM chat_rooms");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
 
   // fetching all User status table
    public function getUserStatus() {
        $stmt = $this->conn->prepare("SELECT * FROM user_status");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
  
  
   // fetching all User status table
    public function getUserStatusOnId($userid) {
		 
		 
		     $stmt = $this->conn->prepare("SELECT status,time FROM user_status WHERE user_id=?");
			 $stmt->bind_param("i",$userid);
			 
			  $st;			   
			   
			   if ($stmt->execute()) {            
			  
			   $stmt->bind_result($status,$time);
               $stmt->fetch();  
               $st=$status;			   
               $stmt->close();           
            } 		
    
      
		
        return $st;
    }
  
  
  

	
	function getChatRoom($chat_room_id) {
	   $response['messages']=array();
	   $response['chat_room'] = array();
	   //echo "hi";
        //$stmt = $this->conn->prepare("SELECT cr.chat_room_id, cr.name, cr.created_at as chat_room_created_at, u.name as username, c.* FROM chat_rooms cr LEFT JOIN messages c ON c.chat_room_id = cr.chat_room_id LEFT JOIN users u ON u.user_id = c.user_id WHERE cr.chat_room_id = ?");
        //$stmt->bind_param("i", $chat_room_id);
		
			 $query="SELECT messages.user_id,messages.chat_room_id
               FROM users
               INNER JOIN messages ON users.user_id = messages.chat_room_id WHERE messages.chat_room_id=? or messages.user_id=? GROUP by messages.user_id";
        //$stmt = $this->conn->prepare("SELECT user_id,name FROM users");
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("ii", $chat_room_id, $chat_room_id);
		$stmt->execute();
        $tasks = $stmt->get_result();
		//foreach ($tasks as $row);
		$user=array();
		$response['chat_room']=$this->getUser($chat_room_id);
		$response["error"] = false;
		while ($row = $tasks->fetch_array())
        {       
				  $temp =array();
				  if($row['user_id']!=$chat_room_id)
				  {
				  $temp=$this->getUser($row['user_id']);				 
				  array_push($user,$temp);
				  }else
				  {
					   $temp=$this->getUser($row['chat_room_id']);			 
				  array_push($user,$temp);
				  }
				
        }
		$user=$this->my_array_unique($user,true);
		
		 $i=0;
		$message=array();
		while ($i<sizeof($user))
		{
			    $message['user_id']=$user[$i]['user_id'];
				$message['name']=$user[$i]['name']; 
				$message['image']=$user[$i]['image']; 	
				$message['contact']=$user[$i]['contact']; 	
				$message['messages']=$this->getUserMessage($chat_room_id,$user[$i]['user_id']);
				if(!empty($message['messages']))
				{
				array_push($response['messages'],$message);
				}
				$i++;
		}
		
	      echoRespnse(200,$response);
        $stmt->close();
       // return $tasks;
    }
	
	function my_array_unique($array, $keep_key_assoc = false){
    $duplicate_keys = array();
    $tmp = array();       

    foreach ($array as $key => $val){
        // convert objects to arrays, in_array() does not support objects
        if (is_object($val))
            $val = (array)$val;

        if (!in_array($val, $tmp))
            $tmp[] = $val;
        else
            $duplicate_keys[] = $key;
    }

    foreach ($duplicate_keys as $key)
        unset($array[$key]);

    return $keep_key_assoc ? $array : array_values($array);
}

	function getUserMessage($chat_room_id,$user_id)
	{
		    
		    $stmt1 = $this->conn->prepare("SELECT * FROM `messages` WHERE chat_room_id=? and user_id=?");
			 $stmt1->bind_param("ii", $chat_room_id,$user_id);
			  $stmt1->execute();
           //  print_r($stmt1);			  
              $tasks1 = $stmt1->get_result();
             $message=array();			  
			  	while ($chat_room = $tasks1->fetch_assoc())
             {
				 	$user=array();       
					$user["message"] = $chat_room["message"];
					$user["file"] = $chat_room["file"];
                    $user["message_id"] = $chat_room["message_id"];
                    $user["local_id"] = $chat_room["local_id"];
                    $user["created_at"] = $chat_room["created_at"];
					array_push($message,$user);
			 }	
           		  
			  return $message;
			  $stmt1->close();
	}
 
    /**
     * Fetching user by contact
     * @param String $contact User contact id
     */
    public function getUserByContact($contact) {
        $stmt = $this->conn->prepare("SELECT user_id, name, contact,password,created_at,image,about FROM users WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($user_id, $name, $contact,$password, $created_at,$image,$about);
            $stmt->fetch();
            $user = array();
            $user["user_id"] = $user_id;
            $user["name"] = $name;
            $user["contact"] = $contact;
			$user["password"] = $password;
            $user["created_at"] = $created_at;	
		    $user["image"]=$image;	
			$user["about"]=$about;
			
		    
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
    
	
	    public function getUserByContactConfirmOTP($contact) {
        $stmt = $this->conn->prepare("SELECT user_id,otp FROM users WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($user_id,$otp);
            $stmt->fetch();
            $user = array();
            $user["user_id"] = $user_id;
            $user["otp"] = $otp;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }


public function userUpdate($id,$name,$about,$path) {


   echo "In db Update";
        
       $response;
    
       $stmt = $this->conn->prepare("UPDATE users SET  name = ?, about = ?,image = ? WHERE user_id=?");
       
            $stmt->bind_param("sssi",$name,$about,$path,$id);
 
            $result = $stmt->execute();
           //print_r($stmt);
            $stmt->close();
             
            // Check for successful insertion
            if ($result) {

            $response['image'] = $path;
            $response['about']=$about;
            $response['name']=$name;        

             echo "Done db Update";       
            
            }else {
               // Failed to create user
               $response="FailtoUpdate";
            }
      // return $response;
   // echoRespnse(200,$response);
    }





function echoRespnse($status_code, $response) {
    
    
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
    
}






	
	//-------------------------------------------------------------------------
 //Connect Go API Start
 
 private function isContactExists_ConnectGo($contact) {
        $stmt = $this->conn->prepare("SELECT id from driver_data WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
       return $num_rows > 0;
    }

//registerWithOTP Connect Go

     public function registerWithOTP_ConnectGo($contact,$contactwithoutcontrycode) {
        $response = array();
 
$result=$this->isContactExists_ConnectGo($contact);
//echo $result;
 // First check if user already existed in db     
          if ($result==0) {
		
			//Generating a 6 Digits OTP or verification code 
			     $otp = rand(100000, 999999);
				 
		    // insert query
            $stmt = $this->conn->prepare("INSERT INTO driver_data(contact,otp) values(?,?)");
            $stmt->bind_param("si", $contact ,$otp);
 
            $result = $stmt->execute();
         //  print_r($stmt);
            $stmt->close();
			
      
            // Check for successful insertion
            if ($result) {
				
              // $response['url'] = $file_url;
               echo $response=$this->sendOtp($otp,$contactwithoutcontrycode,$contact);
					
					$response["success"]=true;
			        $response["otp"]=$otp;										  
            } 
			else 
			{
               // Failed to create user
               $response[]="Failedtocreate";
            } 
        }
		else {
            // User with same contact already existed in the db
		//	echo "hi";
		//   $response["success"]=false;
	     $response[]="ContactAlreadyExist";
          
        }

        return $response;
    }
	
	
	  // creating new user if not existed
    public function ConfirmOTP_ConnectGo($userEnterOtp,$contact) {
        $response = array();
 
       $response = $this->getUserByContactConfirmOTP_ConnectGo($contact);
   
      if($response["otp"]==$userEnterOtp){
	      $response["success"] =true;
	  }else{
	     $response["success"] =false;
	  }
 
        return $response;
    }
	
	//getting user by contact
	    public function getUserByContactConfirmOTP_ConnectGo($contact) {
        $stmt = $this->conn->prepare("SELECT id,otp FROM driver_data WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($id,$otp);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["otp"] = $otp;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
	
		
    // LOg in 
    public function LogIn_ConnectGo($contact, $password) {
		
		//echo "hi in echo ".$contact." ".$password;
        $response = array();
 
 $result=$this->isUserExists_ConnectGo($contact,$password);
 
 //echo $result;
        // First check if user already existed in db

        if ($result>=1) {  
		
            $response["error"] = false;
			$response["success"]=true;
            $response["user"] = $this->getUserByContact_ConnectGo($contact);
		
        }
		else
		{
			   // User with same contact already existed in the db
       
			$response["error"] = true;
			$response["success"]=false;
            $response["message"] = 'contact or password is not valid';
            // echoRespnse(400, $response);
		}
 
        return $response;
	
    }
	
	 /**
     * Fetching user by contact
     * @param String $contact User contact id
     */
    public function getUserByContact_ConnectGo($contact) {
        $stmt = $this->conn->prepare("SELECT id, name, contact,city,password,profile_pic,licence,address_proof,id_proof,car_document,otp,working_status,car_company_name,car_name,car_year,car_number FROM driver_data WHERE contact = ?");
        $stmt->bind_param("s", $contact);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($id, $name, $contact,$city, $password,
			$profile_pic,$licence, $address_proof,$id_proof,$car_document,
			$otp,$working_status,$car_company_name,$car_name,$car_year,$car_number);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["name"] = $name;
            $user["contact"] = $contact;
			$user["city"] = $city;
            $user["password"] = $password;	
		    $user["profile_pic"]=$profile_pic;	
			$user["licence"]=$licence;
			$user["address_proof"]=$address_proof;
			$user["id_proof"]=$id_proof;
			$user["car_document"]=$car_document;
			$user["otp"]=$otp;
			$user["working_status"]=$working_status;
			$user["car_company_name"]=$car_company_name;
			$user["car_name"]=$car_name;
			$user["car_year"]=$car_year;
			$user["car_number"]=$car_number;
			
			
			
		    
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
	
	//Check user exits in connect go
	
	  private function isUserExists_ConnectGo($contact,$password) {
        $stmt = $this->conn->prepare("SELECT id from driver_data WHERE contact = ? and password= ?");
        $stmt->bind_param("ss", $contact,$password);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
 
 
 // Register data of user all info 
 
 
		public function RegisterDriver($username,$contact,$city,$password,$file_url,$licence_url,$address_proof_url,
		                               $id_proof_url,$car_document_url,$carCompanyName,$carName,$carYear,$carnumber) {
        
         $response="";
		
		  
		/* echo $username." ".$contact." ".$city." ".$password." "
		 .$file_url." ".$licence_url." ".$address_proof_url." "
		 .$id_proof_url." ".$car_document_url." ".$carcapacity." "
		 .$carname." ".$carnumber;
		 */
		 
        // First check if user already existed in db
      //  if ($this->isContactExists($contact)) {
			

            // insert query
			$status=false;
			$otp=null;
            $stmt = $this->conn->prepare("UPDATE  driver_data 
			SET name=?,city=?,password=?,profile_pic=?,licence=?,address_proof=?,id_proof=?,car_document=?,working_status=?, otp=?, car_company_name=?, car_name=?, car_year=?,car_number=? where contact=?");
            $stmt->bind_param("sssssssssssssss", $username,$city,$password,$file_url,$licence_url,$address_proof_url,$id_proof_url,
			                                    $car_document_url,$status,$otp,$carCompanyName,$carName,$carYear,$carnumber,$contact);    
			$result = $stmt->execute();
					
			
         //  print_r($stmt);
            $stmt->close();
             
            // Check for successful insertion
           if ($result) 
			{
             
               $response="Success";	
			 
            } 
			else 
			{
               // Failed to create user
               $response="Failedtocreate";
            } 
			
			
			
       /* }
		else 
		{
            // User with same contact already existed in the db
		//	echo "hi";
           $response="Numberisnotverified";
        }*/
		
 
       return $response;
	//  echoRespnse(200,$response);
    }

    		
			public function UpdateDriverWorkingStatus($contact,$status) {
        
             $response= array();	
   			 
		
           
            $stmt = $this->conn->prepare("UPDATE  driver_data 
			SET working_status=? where contact=?");
            $stmt->bind_param("ss",$status,$contact);    
			$result = $stmt->execute();
					
             
           
           if ($result) 
			{             
               $response["status"]=$status;				 
            } 
			else 
			{             
               $response["status"]="Failedtocreate";
            }
       
		
 
       return $response;
	
    }
	
	
	
	 public function getAllCarCompany() {
        $stmt = $this->conn->prepare("SELECT * FROM car_company");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
	
	
	
	public function getAllCarsOnCompanyId($id) {
        $stmt = $this->conn->prepare("SELECT * FROM car_names where company_id=?");
		$stmt->bind_param("i",$id);    
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
	
	public function getAllCarCompanyUSA() {
        $stmt = $this->conn->prepare("SELECT * FROM CarCompanyNameUSA");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
	
	
	public function getAllCarsOnCompanyIdUSA($id) {
        $stmt = $this->conn->prepare("SELECT * FROM CarNamesUSA where usa_company_id=?");
		$stmt->bind_param("i",$id);    
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

}
 
?>