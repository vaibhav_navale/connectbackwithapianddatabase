<?php
 
/**
 * Handling database connection
 *
 */
class DbConnect {
 
    private $conn;
 
    function __construct() {        
    }
 
    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once dirname(__FILE__) . '/config.php';
 
        // Connecting to mysql database
     // $this->conn = new mysqli(hostname, user, password, databaseName);
	  
	  $this->conn = new mysqli("localhost", "connect", "connect", "con_app");
 
        // Check for database connection error
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
      // returing connection resource
        return $this->conn;
    }
}
?>