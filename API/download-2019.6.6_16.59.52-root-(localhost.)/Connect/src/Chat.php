<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

require "../constant.php";
error_reporting(-1);

ini_set('display_errors', 'On'); 
require_once '../include/db_handler.php';
require_once '../libs/gcm/gcm.php';
require_once '../libs/gcm/push.php';


class Chat implements MessageComponentInterface {
    protected $clients;
	private $onlineusers;
    private $users;
    private $touser;
	private $conn;
	private $mainarrayuser;
	private $mobileuser;
	private $temp;
	
 // public $dbconn;

    public function __construct() {
            $this->clients = new \SplObjectStorage;
			echo "server started";
			$this->mainarrayuser = [];
			$this->mobileuser = [];
			$this->onlineusers = [];			 
            $this->users = []; 						
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
       $this->updateOnclose($conn->resourceId,1);
    }

    public function onMessage(ConnectionInterface $from, $msg) {	
    
		
        $data = json_decode($msg, true);
		
      // print_r($data);

        
        switch ($data[FOR_MAIN_CLIENT]) {

            case STORE:
                switch ($data[CHANNEL]) {
                    case CH_MOBILE:
                        $this->storeMobileClients($from, $data);
                        break;
                    case CH_WEB:
                        //$this->storeWebClients($from, $data);
                        break;
                    default:
                        break;
                }
                break;
            case SEND:
                
                    switch ($data[TO]) {
                        case CH_MOBILE:
                            $this->sendDataToMobilValidClient($from, $data);
                            break;
                        case CH_WEB:
                            //$this->sendDataToWebValidClient($from, $data);
                            break;
                        default:
                            break;
                    }
                
                break;
            default:
                break;
        }        

     //   print_r("mainarrayuser");
        //print_r($this->mainarrayuser);
				
    }//close of onMessage()
	


	
	//store all mobileClient who are connected to websocket
    private function storeMobileClients($from, $data) {

        $onlineusers = array();
                
        for ($i = 0; $i < count($data[TYPE]); $i++) {

            switch ($data[TYPE][$i]) {

                case SHOW_ONLINEUSER:

                    for ($i=0;$i<sizeof($this->onlineusers);$i++){

                        $val=$this->onlineusers[$i];
                      if($val["user_id"]==$data["user_id"]){
                           unset($this->onlineusers[$i]);
                          $this->onlineusers=array_values( $this->onlineusers);
                          break;
                      }
                    }
                    $onlineusers["resourceId"] = $from->resourceId;
                    $onlineusers["user_id"] = $data["user_id"];
                    array_push($this->onlineusers, $onlineusers);
                     print_r($this->onlineusers);

                    break;                
                default:
                    break;
            }
        }

        $this->mobileuser['onlineusers'] = $this->onlineusers;      
        $this->mainarrayuser['mobileuser'] = $this->mobileuser;
    }

	
	//send to mobileClient who are connected to websocket
    private function sendDataToMobilValidClient($from, $data) {
        for ($i = 0; $i < count($data[TYPE]); $i++) {
            switch ($data[TYPE][$i]) {
                case SHOW_ONLINEUSER:
                    $this->sendMessageDataToMobileUser($from, $data);
                    break;
               case SEND_STATUS:
			        $this->sendStatus($from,$data);
				   break;  
				   
		       case SEND_DELIVERY:
			        $this->sendDelivery($from,$data);
				   break;
			   case SEND_PROFILEDATA:
			        $this->sendProfileUpdate($from,$data);
				   break;
                default:
                    break;
            }
        }
    }
	
	
	private function sendMessageDataToMobileUser($from, $data){
		
		
		
		if (count($this->mainarrayuser) > 0) {

        $this->temp=false;	
        
            foreach ($this->mainarrayuser["mobileuser"]["onlineusers"] as $key => $value) {

			
			
                if ($data['user_id'] == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["user_id"]) {

                    foreach ($this->clients as $client) {

                        if ($client->resourceId == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["resourceId"]) {
                            if ($from !== $client) {
								
								    $this->temp=true;								
									$allData=$this->getAllData($data,false);	
								//	print_r($allData);
                                    $client->send(json_encode($allData));
                            }
                        }
                    }
                }
				
            }//end of foreach
        }//end of main if
		
		if($this->temp){
			echo " User Available msg in send message method\n\n";		
		}else{			
			              echo " User Not Available msg in send message method \n\n";            				  
			              //	$this->sendMessageViaFCM($data);
                          $gcm = new \GCM();
                          $push = new \Push();
						  $push->setTitle("Connect");
						  $push->setIsBackground(FALSE);
						  $push->setFlag(2);
						  $allData=$this->getAllData($data,true);  
						  $push->setData($allData); 
                          $gcm->send( $allData['to_user']['gcm_registration_id'], $push->getPush());					  
					      echo "Send \n\n";

			
             }//end if else
		 
  }//end of send Message Data To MobileUser method
  
  
  public function sendStatus($from,$data){	


			  
			  
			  $db=new \DbHandler();	  
			

			  $innerData = $data['data'];
			    //print_r($innerData);
			 
			
			  $id=$innerData["from_user_id"];
			  $status=$innerData["message"];
			  $epoch=$innerData["epoch"];
			  
			  $db->UserStatusFromWebSocket($id,$status,$epoch);
	                   
	      foreach ($this->clients as $client) {
                       
                            if ($from !== $client) {								
								    $client->send(json_encode($data));							
                            }                        
                    }

   }// end of sendMessageViaFCM method
	


	
	 public function sendDelivery($from,$data){	
	 
	if (count($this->mainarrayuser) > 0) {

        $this->temp=false;	
        
            foreach ($this->mainarrayuser["mobileuser"]["onlineusers"] as $key => $value) {

			
			
                if ($data['user_id'] == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["user_id"]) {

                    foreach ($this->clients as $client) {

                        if ($client->resourceId == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["resourceId"]) {
                            if ($from !== $client) {
								
								    $this->temp=true;			
							        $client->send(json_encode($data));
                            }
                        }
                    }
                }
				
            }//end of foreach
        }//end of main if
		
		if($this->temp){
			echo " User Available msg in send delivery method \n\n";		
		}else{			
		
		      $db=new \DbHandler();	
		
		       $userid = $data['user_id'];    
			
			   $to_User=$db->getUser($userid); 	    
			              echo " User Not Available msg in send delivery method\n\n";            				  
			       
				   
				   
                          $gcm = new \GCM();
                          $push = new \Push();
						  $push->setTitle("Connect");
						  $push->setIsBackground(FALSE);
						  $push->setFlag(2);						 
						  $push->setData($data); 		  
						  
                          $gcm->send( $to_User['gcm_registration_id'], $push->getPush());					    
				
					   
						  

			
             }//end if else
           
	 }


	  public function sendProfileUpdate($from,$data){	


			 
			  
			  $db=new \DbHandler();	  
			

			  $innerData = $data['data'];
			    //print_r($innerData);
			 
			
			  $id=$innerData["from_user_id"];
			  $name=$innerData["name"];
			  $about=$innerData["about"];
			  $path=$innerData["fileparam"];
			  
			  $db->userUpdate($id,$name,$about,$path);

		
	                   
	      foreach ($this->clients as $client) {
                       
                            if ($from !== $client) {								
								    $client->send(json_encode($data));							
                            }                        
                    }

   }// end of sendProfileUpdate method
	
	
	
	
	public function sendRetriveMessage($from,$from_User_Id,$toSendData){
		
		   if (count($this->mainarrayuser) > 0) {

            foreach ($this->mainarrayuser["mobileuser"]["onlineusers"] as $key => $value) {

                if ($from_User_Id == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["user_id"]) {
               // echo " Sapdla...   1  \n\n";
                    foreach ($this->clients as $client) {
					//	echo " Sapdla...   2  \n\n";
						//print_r($client);

                        if ($client->resourceId == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["resourceId"]) {
                      //   echo " Sapdla...   3  \n\n";
						 if ($from == $client) {					       
                                   $client->send(json_encode($toSendData));								  
							}							
                           }
                        }
                    }else{					
					
						 
					}
                }
            }//end of if
		
		
	}//end of sendRetriveMessage
	
	public function getAllData($data,$checkFCMOrWebocket){
		
		$to_User_Id = $data['user_id'];
          $userids = $data['data'];

          if(!empty($userids)){

				// get ids from givin data array
                $from_User_Id = $userids['from_user_id'];
                $message = $userids['message'];
                $file_path = $userids['fileparam'];
				$statuscode=$data['statuscode'];
                $epochtime = $userids['epoch'];
                $local_id = $userids['local_id'];
				$db=new \DbHandler();		
				// add to local and get response
			   $response = $db->addMessage($from_User_Id, $to_User_Id, $message,$file_path,$local_id,$epochtime);		 
			   
			   $From_User=$db->getUser($from_User_Id);
			   $To_User=$db->getUser($to_User_Id); 	        
	          //Add to data array
			 $allData['title']	="connect";
             $allData['to_user'] = $To_User;       
        	 $messagearray=array();       	 
             array_push($messagearray,$response["message"]);
             $allData['error'] = $response['error'];
             $allData['statuscode'] = $statuscode;
			// print_r($response);
		     $allData['message'] = $messagearray;	  
             $allData['from_user']	=$From_User;
             $allData['deliver']	="not";
             $allData['fcmorweb']	=$checkFCMOrWebocket;

			
   
          }  
		
		return $allData;
	}
    
	public function onClose(ConnectionInterface $conn) {        
        
        echo "Connection {$conn->resourceId} has disconnected\n";
        $this->updateOnclose($conn->resourceId,false);
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }


    public function updateOnclose($resourceId, $status){


        $innerData=array();





     $check=false;

        for ($i=0;$i<sizeof($this->onlineusers);$i++) {
            $val = $this->onlineusers[$i];
            if ($val["resourceId"] == $resourceId) {
                $db = new \DbHandler();
                
                $epoch = round(microtime(true) * 1000);

                $db->UserStatusFromWebSocket($val["user_id"], $status, $epoch);

                $innerData["from_user_id"]=$val["user_id"];
                $innerData["message"]=$status;
                $innerData["epoch"]=$epoch;
                $check=true;
                break;
            }
          }

        if($check){
            $data=array();
            $type=array();


            $type[0]=SEND_STATUS;
            $data["for"]=SEND;
            $data["to"]=TO_MOBILE;
            $data["statuscode"]=STATUS;
            $data["type"]= $type;
            $data["data"]=$innerData;
            foreach ($this->clients as $client) {
                    $client->send(json_encode($data));
            }

          }

        }






}
