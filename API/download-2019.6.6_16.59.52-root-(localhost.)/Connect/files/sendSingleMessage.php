<?php
 
error_reporting(-1);
ini_set('display_errors', 'On');
 
require_once '../include/db_handler.php';
require_once '../libs/gcm/gcm.php';
require_once '../libs/gcm/push.php';
require '.././libs/Slim/Slim.php';
 
Slim\Slim::registerAutoloader();
 
    $app = new Slim\Slim();
    $db=new DbHandler();
   
       
         // verifyRequiredParams(array('message'));
		  
		
		$from_user_id = $app->request->post('user_id');
     	$to_user_id   = $app->request->post('to_user_id');
	    $message = $app->request->post('message');
	    
      	 /*   $from_user_id = "1";
     	$to_user_id   = "34";
	    $message = "Hello";*/
	   
	    
	    
        $response = $db->addMessage($from_user_id , $to_user_id, $message,"");
			

	   $gcm = new GCM();
       $push = new Push();
		   
        $to_user = $db->getUser($to_user_id);
	    $from_user = $db->getUser($from_user_id );

	    $data = array();
        $data['to_user'] = $to_user;
		$data['message'] = $response['message'];	  
        $data['from_user']	=$from_user;	
		

        $push->setTitle("Connect");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_USER);
        $push->setData($data); 

      
       $gcm->send($to_user['gcm_registration_id'], $push->getPush());
	  
	      $response['user'] = $to_user;
		  $response['from_user'] = $from_user;
          $response['error'] = false;
	
    
    
    
    function echoRespnse($status_code, $response) {
        
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}

?>