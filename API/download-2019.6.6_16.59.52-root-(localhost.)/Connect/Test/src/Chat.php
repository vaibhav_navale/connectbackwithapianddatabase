<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
require  "../db/Users.php";
require  "../db/chatrooms.php";
require "../constant.php";


class Chat implements MessageComponentInterface {

    protected $clients;
	private $onlineusers;
    private $users;
    private $touser;
	private $conn;
	private $mainarrayuser;
	private $mobileuser;
 // public $dbconn;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
			echo "server started";
			$this->mainarrayuser = [];
			$this->mobileuser = [];
			$this->onlineusers = [];			 
                        $this->users = []; 
                  
		//require_once("/opt/lampp/htdocs/WebSocket/db/DbConnect.php");
		// $db = new DbConnect();
		// $this->dbconn=$db->connect();
		
			
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";		  
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        
		print_r("message" . $msg);
        $data = json_decode($msg, true);

        print_r($data);

         
	 

            
          

          $to_User_Id = $data['user_id'];
          $userids = $data['data'];

          if(!empty($userids)){
           $objchatrooms =new  \chatrooms();
                $from_User_Id = $userids['from_user_id'];
                $message = $userids['message'];
                $epoch = $userids['epoch'];
                $local_id = $userids['local_id'];

               
               $From_User=$objchatrooms->getUsersById($from_User_Id);
            //   
                print_r($From_User);
               

             //   print_r("from_User_Id : ".$from_User_Id." message : ".$message." epoch : ".$epoch." local_id : ".$local_id."
             //   to_User_Id : ".$to_User_Id);


          }         
          

                   

        
        switch ($data[FOR_MAIN_CLIENT]) {

            case STORE:
                switch ($data[CHANNEL]) {
                    case CH_MOBILE:
                        $this->storeMobileClients($from, $data);
                        break;
                    case CH_WEB:
                        //$this->storeWebClients($from, $data);
                        break;
                    default:
                        break;
                }
                break;
            case SEND:
                
                    switch ($data[TO]) {
                        case CH_MOBILE:
                            $this->sendDataToMobilValidClient($from, $data);
                            break;
                        case CH_WEB:
                            //$this->sendDataToWebValidClient($from, $data);
                            break;
                        default:
                            break;
                    }
                
                break;
            default:
                break;
        }        

        print_r("mainarrayuser");
        print_r($this->mainarrayuser);
				
    }//close of onMessage()
	






	
	//store all mobileClient who are connected to websocket
    private function storeMobileClients($from, $data) {

        $onlineusers = array();
                
        for ($i = 0; $i < count($data[TYPE]); $i++) {

            switch ($data[TYPE][$i]) {

                case SHOW_ONLINEUSER:

                    $onlineusers["resourceId"] = $from->resourceId;
                    $onlineusers["user_id"] = $data["user_id"];
                    array_push($this->onlineusers, $onlineusers);
                    break;                
                default:
                    break;
            }
        }

        $this->mobileuser['onlineusers'] = $this->onlineusers;      
        $this->mainarrayuser['mobileuser'] = $this->mobileuser;
    }

	
	//send to mobileClient who are connected to websocket
    private function sendDataToMobilValidClient($from, $data) {
        for ($i = 0; $i < count($data[TYPE]); $i++) {
            switch ($data[TYPE][$i]) {
                case SHOW_ONLINEUSER:
                    $this->sendMessageDataToMobileUser($from, $data);
                    break;                
                default:
                    break;
            }
        }
    }
	
	
	private function sendMessageDataToMobileUser($from, $data){
		if (count($this->mainarrayuser) > 0) {

            foreach ($this->mainarrayuser["mobileuser"]["onlineusers"] as $key => $value) {

                if ($data['user_id'] == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["user_id"]) {

                    foreach ($this->clients as $client) {

                        if ($client->resourceId == $this->mainarrayuser["mobileuser"]["onlineusers"][$key]["resourceId"]) {
                            if ($from !== $client) {
                                $client->send(json_encode($data));
                            }
                        }
                    }
                }
            }
        }
	}
    
	public function onClose(ConnectionInterface $conn) {        
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";		
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
