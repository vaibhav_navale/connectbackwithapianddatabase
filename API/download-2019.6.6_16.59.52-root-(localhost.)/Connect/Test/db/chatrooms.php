<?php
 class chatrooms{
  
  private $id;
  private $userId;
  private $msg;
  private $created_on;
  public $dbconn;
  
  
  function setId($id){ $this->id=$id; }
  function getId(){ return $this->id; }
  
  function setUserId($userId){ $this->userId=$userId; }
  function getUserId(){ return $this->userId; }
  
  function setMsg($msg){ $this->msg=$msg; }
  function getMsg(){ return $this->msg; }
  
  function setCreatedOn($created_on){ $this->created_on=$created_on; }
  function getCreatedOn(){ return $this->created_on; }
  


     public function __construct(){
		 
		 require_once("DbConnect.php");
		 $db = new DbConnect();
		 $this->dbconn=$db->connect();
	 }//close of constructor
	 
	 
    

    public function save(){
		 
		 $sql = "INSERT INTO chatrooms(id, userId , msg , created_on) VALUES (null, :userId, :msg, :created_on) ";
		 
		 $stmt = $this->dbconn->prepare($sql);
		 
		 $stmt->bindParam(":userId", $this->userId);
		 $stmt->bindParam(":msg", $this->msg);
		 $stmt->bindParam(":created_on", $this->created_on);
		
		 try{
			 
			 if($stmt->execute()){				 
				 return true;				 
         	 }else{				 
			    return false;				
			}
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	
		 
	 }//close of Save()
	 
	 
	  
	 /*
****
this function is used to get All chatrooms
****
*/	
	 
	  public function getAllChatRoom(){
		 
		 $sql = "select c.*,u.name from chatrooms c JOIN users u ON(c.userId = u.id)";		 
		 $stmt = $this->dbconn->prepare($sql);	
		 
		 try{			 
			 if($stmt->execute()){			 
				$chatrooms=$stmt->fetchAll(PDO::FETCH_ASSOC);
				 return $chatrooms;
				 }			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	
		 
	 }//close of getAllChatrooms()


/*
****
this function is used to get User by there  id
****
*/	 
	 
	 public function getUsersById($id){
		 
		 $sql = "select * from users where id= :id";
		 
		 $stmt = $this->dbconn->prepare($sql);		 
		
		 $stmt->bindParam(":id",$id);
		
		 
		 try{
			 
			 if($stmt->execute()){	
		 
				$user=$stmt->fetch(PDO::FETCH_ASSOC);
				 return $user;
         	 }
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	
		 
	 }//close of getUsersByID()



	
		 
}//close of class
	 
?>
