<?php


error_reporting(-1);
ini_set('display_errors', 'On');

  class Users{
  
  private $id;
  private $name;
  private $email;
  private $loginstatus;
  private $lastlogin;
  public $dbconn;

  
  
  function setId($id){ $this->id=$id; }
  function getId(){ return $this->id; }  

  
  function setEmail($email){ $this->email=$email; }
  function getEmail(){ return $this->email; }
  
  function setName($name){ $this->name=$name; }
  function getName(){ return $this->name; }
  
  function setLoginstatus($loginstatus){ $this->loginstatus=$loginstatus; }
  function getLoginstatus(){ return $this->loginstatus; }
  
  function setLastlogin($lastlogin){ $this->lastlogin=$lastlogin; }
  function getLastlogin(){ return $this->lastlogin; }
  

     public function __construct(){
		 
		 require_once("DbConnect.php");
		 $db = new DbConnect();
		 $this->dbconn=$db->connect();
		  
		 
	 }




/*
****
this function is used to get User by there  id
****
*/	 
	 
	 public function getUsersById($id){
		 
		 $sql = "select * from users where id= :id";
		 
		 $stmt = $this->dbconn->prepare($sql);		 
		
		 $stmt->bindParam(":id",$id);
		 
		 
		 try{
			 
			 if($stmt->execute()){	
		 
				$user=$stmt->fetch(PDO::FETCH_ASSOC);
				 return $user;
         	 }


			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	

		 echo $stmt->error;
		 
	 }//close of getUsersByID()






	 
/*
****
this function is used to save the data in database table users
****
*/	 
	 public function save(){
		 
		 $sql = "INSERT INTO users(id, name , email , login_status, last_login) VALUES (null, :name, :email, :loginstatus, :lastlogin) ";
		 
		 $stmt = $this->dbconn->prepare($sql);
		 
		 $stmt->bindParam(":name", $this->name);
		 $stmt->bindParam(":email", $this->email);
		 $stmt->bindParam(":loginstatus", $this->loginstatus);
		 $stmt->bindParam(":lastlogin", $this->lastlogin);
		 
		 try{
			 
			 if($stmt->execute()){				 
				 return true;				 
         	 }else{				 
			    return false;				
			}
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	
		 	 
		 
	 }//close of Save()

	 
 /*
****
this function is used to get User by there email id
****
*/
      	 
	 public function getUserByEmail(){

	 	echo "In get email";
		 
		 $sql = "select * from users where email= :email";
		 
		 $stmt = $this->dbconn->prepare($sql);		 
		
		 $stmt->bindParam(":email", $this->email);
		
		 
		 try{
			 
			 if($stmt->execute()){	
		 
				$user=$stmt->fetch(PDO::FETCH_ASSOC);
				 return $user;
         	 }
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	

		 	 echo $stmt->error;
		 	 echo "Complete get email";
		 
	 }//close of getUserByEmail()
	 

/*
****
this function is used to get User by there  id
****
*/	 
	 
	 public function getUserById(){
		 
		 $sql = "select * from users where id= :id";
		 
		 $stmt = $this->dbconn->prepare($sql);		 
		
		 $stmt->bindParam(":id", $this->id);
		
		 
		 try{
			 
			 if($stmt->execute()){	
		 
				$user=$stmt->fetch(PDO::FETCH_ASSOC);
				 return $user;
         	 }
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	

		 	 echo $stmt->error;
		 
	 }//close of getUserByID()

/*
****
this function is used to update user login status
****
*/		 
	 
	 
	  public function updateLoginStatus(){
		 
		 $sql = "update  users set login_status=:loginstatus, 
		            last_login=:lastlogin where id=:id";
		 
		 $stmt = $this->dbconn->prepare($sql);				
		 
		 $stmt->bindParam(":loginstatus", $this->loginstatus);
		 $stmt->bindParam(":lastlogin", $this->lastlogin);
		  $stmt->bindParam(":id", $this->id);
		 
		 
		 try{
			 
			 if($stmt->execute()){				 
				return true;
         	 }else{
				 return false;
			 }
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	

		 	 echo $stmt->error;
		 
	 }//close of updateLoginStatus()
	 
	 
	 
	 	
	 
	 /*
****
this function is used to get All users
****
*/	
	 
	  public function getAllUsers(){
		 
		 $sql = "select * from users";		 
		 $stmt = $this->dbconn->prepare($sql);	
		 
		 try{			 
			 if($stmt->execute()){			 
				$users=$stmt->fetchAll(PDO::FETCH_ASSOC);
				 return $users;
				 }			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	

		 	 echo $stmt->error;
		 
	 }//close of getAllUsers()
	 
	 
	 
	 
/*
****
this function is used to update user login status
****
*/		 
	 
	 
	  public function updateResourceId(){
		 
		 $sql = "update  users set resource_id=:resource_id
		         where id=:id";
		 
		 $stmt = $this->dbconn->prepare($sql);				
		 
		 $stmt->bindParam(":resource_id", $this->resource_id);
		 $stmt->bindParam(":id", $this->id);
		 
		 
		 try{
			 
			 if($stmt->execute()){				 
				return true;
         	 }else{
				 return false;
			 }
			 
		 }catch(Exception $e){			 			 
			 echo $e->getMessage();			 
		 }//close catch	

		 	 echo $stmt->error;
		 
	 }//close of updateLoginStatus()
	 
	 
	 
	 	
  
  
  }

?>
