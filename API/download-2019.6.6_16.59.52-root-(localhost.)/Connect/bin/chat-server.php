<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\Chat;

    require dirname(__DIR__) . '/vendor/autoload.php';

     $wsServer = new WsServer( new Chat());
     $server = IoServer::factory(
        new HttpServer(
            $wsServer
       
        ),
        8081
    );
   $wsServer->enableKeepAlive($server->loop, 5);
   $server->run();
