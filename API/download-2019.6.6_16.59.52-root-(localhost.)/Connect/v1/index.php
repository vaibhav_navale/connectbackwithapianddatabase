<?php
 
error_reporting(-1);
ini_set('display_errors', 'On');
 
require_once '../include/db_handler.php';
require_once '../libs/gcm/gcm.php';
require_once '../libs/gcm/push.php';
require '.././libs/Slim/Slim.php';
 
Slim\Slim::registerAutoloader();
 
$app = new Slim\Slim();
 



//----------------------------------------------------------------------------------------
 
/**
 * Deleting message 
 * Deleting Message
 * * */

 
 //----------------------------User delete message start-------------------------------------//
// User delete message

$app->post('/deleteMessage', function() use ($app) {
	
	
    // check for required params
    verifyRequiredParams(array('user_id', 'chatroom_id'));
    // reading post params
    $user_id = $app->request->post('user_id');
    $chatroom_id = $app->request->post('chatroom_id');
    $db = new DbHandler();
    $response = $db->deleteMessage($user_id, $chatroom_id);
    echoRespnse(200, $response);
    
    
});


 //----------------------------User delete message end-------------------------------------//
 
 
  //----------------------------User data get-------------------------------------//

$app->post('/getuser', function() use ($app) {
	
	
    // check for required params
    verifyRequiredParams(array('user_id'));
	
 
    // reading post params
    $user_id = $app->request->post('user_id');
   

 
    $db = new DbHandler();
    $user = $db->getUser($user_id);
 

    echoRespnse(200, $user);
});

 //----------------------------End Get User Data-------------------------------------//
 
 
  //----------------------------User delete file start-------------------------------------//
// User delete message
$app->post('/deleteFile', function() use ($app) {
	
    // check for required params
    verifyRequiredParams(array('file_path'));
	
 
    // reading post params
   // $file_path = $app->request->post('file_path');
 
		
		$db_path = $app->request->post('file_path');
		
			$server_ip = gethostbyname(gethostname());
			
	
			$upload_url = 'http://'.$server_ip.'//gcm_chat//';	
				
		
		
            $len = strlen($upload_url);
         $new_path = substr($db_path, $len, strlen($db_path)-$len);
		
        try{
        $return = unlink($new_path);
		$temp=true;
		}catch(Exception $e) 
		{
			$return=false;
			$temp=false;
			
		}
            if($return){
				
			 $response["error"] = false;
             $response["message"] = 'Success';
			 
		}else{
			if($temp)
			{
			     $response["error"] = true;
                 $response["message"] = "Fail";
			}else{
				  $response["error"] = true;
                 $response["message"] = "Already Deleted";
			}
		}
		echoRespnse(200,$response);
	
});

 //----------------------------User delete file end-------------------------------------//

//-----------------------------------send Message start--------------------------------------//
 
/**
 * Sending push notification to a single user
 * We use user's gcm registration id to send the message
 * * */
 
$app->post('/users/:id/message', function($to_user_id) {
    
          global $app;

		  $db=new DbHandler();
          verifyRequiredParams(array('message'));
		  
		
		$from_user_id = $app->request->post('user_id');
	    $message = $app->request->post('message');
        $response = $db->addMessage($from_user_id , $to_user_id, $message,"");
			

	   $gcm = new GCM();
       $push = new Push();
		   
        $to_user = $db->getUser($to_user_id);
	    $from_user = $db->getUser($from_user_id );

	    $data = array();
        $data['to_user'] = $to_user;
		$data['message'] = $response['message'];	  
        $data['from_user']	=$from_user;	
        $data['image'] = '';
		
		//echoRespnse(200, $data);
        $push->setTitle("Connect");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_USER);
        $push->setData($data); 

        // sending push message to single user
      
          $gcm->send($to_user['gcm_registration_id'], $push->getPush());
	  
	      $response['user'] = $to_user;
		  $response['from_user'] = $from_user;
          $response['error'] = false;
		//}
 
});
 
 
 //-----------------------------------send Message end--------------------------------------//

//-----------------------------------------------------------------------------------------
//registration


$app->POST('/reg/register', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('name','contact','password'));
	//echoRespnse(200,"hi");
    
    // //getting name from the request 
	$username = $app->request->post('name');
    $contact = $app->request->post('contact');
    $password = $app->request->post('password');
	 //$imageData = $app->request->post('image');   
	 
	 $db = new DbHandler();
	
	 
	/*  $upload_path = 'Connect/'.$contact.'/';
	 
	
	if (!is_dir($upload_path)) {
                 mkdir($upload_path);         
                    }
					
					
	$server_ip = gethostbyname(gethostname());
	$image_name=$contact.'.png';
	$upload_url = 'http://'.$server_ip.'/gcm_chat/v1/'.$upload_path.$image_name;	
	
	if($imageData!=null || $imageData!="")
	{
		
		
	 $db->RegisterUser($username,$contact,$password,$upload_url);
	 file_put_contents($upload_path.$image_name,base64_decode($imageData));
	 
	 
	}else
	{	
		
		$upload_url="";		
		$db->RegisterUser($username,$contact,$password,$upload_url);
		
	} */
	
	
	 $upload_path = 'Connect/'.$contact.'/';
	
	if (!is_dir($upload_path)) {
                 mkdir($upload_path);         
                    }
	$server_ip = gethostbyname(gethostname());
	
	
	
	if(isset($_FILES['uploadedfile']['name']))
	{
	  $upload_url = 'http://'.$server_ip.'/gcm_chat/v1/'.$upload_path.$_FILES["uploadedfile"]["name"];	
	 	move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $upload_path.$_FILES["uploadedfile"]["name"]);
		 $db->RegisterUser($username,$contact,$password,$upload_url);
	// file_put_contents($upload_path.$image_name,base64_decode($imageData));
	}else
	{
		$upload_url="";		
		$db->RegisterUser($username,$contact,$password,$upload_url);
	} 
});



// User login
$app->post('/user/login', function() use ($app) {
	
	
    // check for required params
    verifyRequiredParams(array('contact', 'password'));
	
 
    // reading post params
    $contact = $app->request->post('contact');
    $password = $app->request->post('password');
	//echo   $contact."   ". $password;
 
    // validating contact address
  //  validatecontact($contact);
 
    $db = new DbHandler();
    $response = $db->createUser($contact, $password);
 
    // echo json response
    echoRespnse(200, $response);
});
 //----------------------------------------------------------------------------------------
//Uplode Video


 $app->POST('/upload_file/:id', function($chat_room_id) use ($app) {  
 
  global $app;
    $db = new DbHandler();
 
    verifyRequiredParams(array('user_id'));	
	
	 $messagearray=array();


  
  for ($i = 0; $i < count($_FILES["file"]); $i++) {
	  
	  

  if (isset($_FILES['file']['name'][$i])) {
	
	
		
		$full_directory_path = '..//Upload' ;
		
		//Checking folder , is already available or not
		if(!is_dir($full_directory_path)){
		
			//Making a new folder
			mkdir($full_directory_path, 0777, true);
		}
	
		
		
		//Specifies where files are saved
		$server_ip = gethostbyname(gethostname());
			$target_path = $full_directory_path . '//' . basename($_FILES['file']['name'][$i]);
			$upload_url = 'http://'.$server_ip.'//gcm_chat//'.$target_path;	
				
		
			if (!move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path)) {
			
				//File failed to be moved to the server , usually because the destination folder is not available
				$response['code'] = 1;
				$response['message'] = "Server error";				
				return;
				
			}
		            // File uploaded successfully
	            	$response['code'] = 2;
		            $response['message'] = "File uploaded successfully!";
					$response['url']=$upload_url;		
        
        }else{
		    $response['code'] = 1;
		    $response['message'] = "File NOt set";		
        } 
		
		 $user_id = $app->request->post('user_id'); 		 		
		
		
	    $message = $app->request->post('message'); 
       
		
		 if(  $response['code'] == 2)
		 { 
	
        $response = $db->addMessage($user_id, $chat_room_id, $message,$response['url']); 		 
         array_push($messagearray,$response["message"]);
		 }
		 
  }
   if ($response['error'] == false) {
        require_once __DIR__ . '/../libs/gcm/gcm.php';
        require_once __DIR__ . '/../libs/gcm/push.php';
        $gcm = new GCM();
        $push = new Push();
 
        // get the user using userid
        $user = $db->getUser($user_id);
		$to_user=$db->getUser($chat_room_id);

           $data = array();
           $data['from_user'] = $user;		   
           $data['to_user'] = $to_user;
		    $data['message'] = $messagearray;
 
           $push->setTitle("Connect");
           $push->setIsBackground(FALSE);
           $push->setFlag(PUSH_FLAG_CHATROOM);
           $push->setData($data);
         
        // echo json_encode($push->getPush());exit;
 
        // sending push message to a topic
           $gcm->sendToTopic('topic_' . $chat_room_id, $push->getPush());
 
           
           }
 
 
  

});
 
 
 
 
 
 
 
 
 
 
 //----------------------------------------------------------------------------------------
//Update
$app->POST('/update', function() use ($app) {
    // check for required params   
	  verifyRequiredParams(array('user_id'));
    //$from_user_id = $app->request->post('user_id');
	
	//$contact = $app->request->post('contact');
    $from_user_id=$app->request->post('user_id');
 
     //getting name from the request 
	$about = $app->request->post('about');
    $name = $app->request->post('name'); 
  //	$imageData=$app->request->post("image");

	 $db = new DbHandler();		 
	  
	
		
	 $upload_path = 'Connect/'.$from_user_id.'/';
	
	if (!is_dir($upload_path)) {
                 mkdir($upload_path);         
                    }
	$server_ip = gethostbyname(gethostname());
	
	
	
	if(isset($_FILES['uploadedfile']['name']))
	{
	  $upload_url = 'http://'.$server_ip.'/gcm_chat/v1/'.$upload_path.$_FILES["uploadedfile"]["name"];	
	 	move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $upload_path.$_FILES["uploadedfile"]["name"]);
		 $db->updateData($from_user_id,$name,$upload_url,$about);		
	}else
	{
		$upload_url="";		
		$db->updateData($from_user_id,$name,$upload_url,$about);	
	} 

});
//-----------------------------------------------------------------------------------------
 
/* * *
 * Updating user
 *  we use this url to update user's gcm registration id 
 */
$app->put('/user/:id', function($user_id) use ($app) {
    global $app;
 
    verifyRequiredParams(array('gcm_registration_id'));
 
    $gcm_registration_id = $app->request->put('gcm_registration_id');
 
    $db = new DbHandler();
    $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
    echoRespnse(200, $response);
});
 
/* * *
 * fetching all chat rooms
 */
$app->get('/chat_rooms', function() {
    $response = array();
    $db = new DbHandler();
 
    // fetching all user tasks
    $result = $db->getAllChatrooms();
 
    $response["error"] = false;
    $response["chat_rooms"] = array();
 
    // pushing single chat room into array
    while ($chat_room = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["chat_room_id"] = $chat_room["chat_room_id"];
        $tmp["name"] = $chat_room["name"];
        $tmp["created_at"] = $chat_room["created_at"];
        array_push($response["chat_rooms"], $tmp);
    }
 
    echoRespnse(200, $response);
});
 
/**
 * Messaging in a chat room
 * Will send push notification using Topic Messaging
 *  */
$app->post('/chat_rooms/:id/message', function($chat_room_id) {
    global $app;
    $db = new DbHandler();
 
    verifyRequiredParams(array('user_id', 'message'));
 
    $user_id = $app->request->post('user_id');
    $message = $app->request->post('message');
 
    $response = $db->addMessage($user_id, $chat_room_id, $message,"");
 
    if ($response['error'] == false) {
        require_once __DIR__ . '/../libs/gcm/gcm.php';
        require_once __DIR__ . '/../libs/gcm/push.php';
        $gcm = new GCM();
        $push = new Push();
 
        // get the user using userid
        $user = $db->getUser($user_id);
 
        $data = array();
        $data['user'] = $user;
        $data['message'] = $response['message'];
        $data['chat_room_id'] = $chat_room_id;
 
        $push->setTitle("Connect");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_CHATROOM);
        $push->setData($data);
         
        // echo json_encode($push->getPush());exit;
 
        // sending push message to a topic
        $gcm->sendToTopic('topic_' . $chat_room_id, $push->getPush());
 
        $response['user'] = $user;
        $response['error'] = false;
    }
 
    echoRespnse(200, $response);
});
 

 
/**
 * Sending push notification to multiple users
 * We use gcm registration ids to send notification message
 * At max you can send message to 1000 recipients
 * * */
$app->post('/users/message', function() use ($app) {
 
    $response = array();
    verifyRequiredParams(array('user_id', 'to', 'message'));
 
    require_once __DIR__ . '/../libs/gcm/gcm.php';
    require_once __DIR__ . '/../libs/gcm/push.php';
 
    $db = new DbHandler();
 
    $user_id = $app->request->post('user_id');
    $to_user_ids = array_filter(explode(',', $app->request->post('to')));
    $message = $app->request->post('message');
 
    $user = $db->getUser($user_id);
    $users = $db->getUsers($to_user_ids);
 
    $registration_ids = array();
 
    // preparing gcm registration ids array
    foreach ($users as $u) {
        array_push($registration_ids, $u['gcm_registration_id']);
    }
 
    // insert messages in db
    // send push to multiple users
    $gcm = new GCM();
    $push = new Push();
 
    // creating tmp message, skipping database insertion
    $msg = array();
    $msg['message'] = $message;
    $msg['message_id'] = '';
    $msg['chat_room_id'] = '';
    $msg['created_at'] = date('Y-m-d G:i:s');
 
    $data = array();
    $data['user'] = $user;
    $data['message'] = $msg;
    $data['image'] = '';
 
    $push->setTitle("Google Cloud Messaging");
    $push->setIsBackground(FALSE);
    $push->setFlag(PUSH_FLAG_USER);
    $push->setData($data);
 
    // sending push message to multiple users
    $gcm->sendMultiple($registration_ids, $push->getPush());
 
    $response['error'] = false;
 
    echoRespnse(200, $response);
});
 
$app->post('/users/send_to_all', function() use ($app) {
 
    $response = array();
    verifyRequiredParams(array('user_id', 'message'));
 
    require_once __DIR__ . '/../libs/gcm/gcm.php';
    require_once __DIR__ . '/../libs/gcm/push.php';
 
    $db = new DbHandler();
 
    $user_id = $app->request->post('user_id');
    $message = $app->request->post('message');
 
    require_once __DIR__ . '/../libs/gcm/gcm.php';
    require_once __DIR__ . '/../libs/gcm/push.php';
    $gcm = new GCM();
    $push = new Push();
 
    // get the user using userid
    $user = $db->getUser($user_id);
     
    // creating tmp message, skipping database insertion
    $msg = array();
    $msg['message'] = $message;
    $msg['message_id'] = '';
    $msg['chat_room_id'] = '';
    $msg['created_at'] = date('Y-m-d G:i:s');
 
    $data = array();
    $data['user'] = $user;
    $data['message'] = $msg;
    $data['image'] = 'https://www.androidhive.info/wp-content/uploads/2016/01/Air-1.png';
 
    $push->setTitle("Google Cloud Messaging");
    $push->setIsBackground(FALSE);
    $push->setFlag(PUSH_FLAG_USER);
    $push->setData($data);
 
    // sending message to topic `global`
    // On the device every user should subscribe to `global` topic
    $gcm->sendToTopic('global', $push->getPush());
 
    $response['user'] = $user;
    $response['error'] = false;
 
    echoRespnse(200, $response);
});
/**
 * Fetching single chat room including single user message the chat messages
 *  */
 $app->get('/get_perticular_message/:chatroomid&:userid', function($chat_room_id,$user_id) {
	
	 
 global $app;
    $db = new DbHandler(); 
     $result['received'] = $db->getUserMessage($chat_room_id,$user_id);
	 $result['send'] = $db->getUserMessage($user_id,$chat_room_id); 
	// $result = $db->getUserMessage($chat_room_id,$user_id);
	echoRespnse(200, $result);
 });
 
 
 
/**
 * Fetching single chat room including all the chat messages
 *  */
$app->get('/chat_rooms/:id', function($chat_room_id) {
    global $app;
    $db = new DbHandler();
 
    $result = $db->getChatRoom($chat_room_id);
	
	//print_r ($result);
   /* $response["error"] = false;
	$response['messages']=array();
	//$response['user']=array();
  //  $response["messages"] = array();
    $response['chat_room'] = array();
	
//	$abc['user']=array();
$i = 0;
  while ($chat_room = $result->fetch_assoc()) {
        // adding chat room node
		//print_r ($chat_room);
		
        if ($i == 0) {
            $tmp = array();
            $tmp["chat_room_id"] = $chat_room["chat_room_id"];
            $tmp["name"] = $chat_room["name"];
            $tmp["created_at"] = $chat_room["chat_room_created_at"];
            $response['chat_room'] = $tmp;
        }
		 if ($chat_room['user_id'] != NULL) {
            
			// user node
			 $user = array();
          // message node 
		 
			$user_new=array();           
            $user_new['user_id'] = $chat_room['user_id'];
            $user_new['username'] = $chat_room['username'];
			$user['user'] = $user_new;		 
            $cmt = array();
            $user["message"] = $chat_room["message"];
            $user["message_id"] = $chat_room["message_id"];
            $user["created_at"] = $chat_room["created_at"];
			
			//$user['messages']=$cmt;
			array_push($response["messages"], $user);
			}
  }
  //echoRespnse(200, $response);*/
 /*   $i = 0;
    // looping through result and preparing tasks array
    while ($chat_room = $result->fetch_assoc()) {
        // adding chat room node
        if ($i == 0) {
            $tmp = array();
            $tmp["chat_room_id"] = $chat_room["chat_room_id"];
            $tmp["name"] = $chat_room["name"];
            $tmp["created_at"] = $chat_room["chat_room_created_at"];
            $response['chat_room'] = $tmp;
        }
 
        if ($chat_room['user_id'] != NULL) {
            // message node
            $cmt = array();
            $cmt["message"] = $chat_room["message"];
            $cmt["message_id"] = $chat_room["message_id"];
            $cmt["created_at"] = $chat_room["created_at"];
 
            // user node
            $user = array();
            $user['user_id'] = $chat_room['user_id'];
            $user['username'] = $chat_room['username'];
            $cmt['user'] = $user;
              array_push($abc['user'],$user);
            array_push($response["messages"], $cmt);
        }
    }
	foreach($abc['user'] as $value){
    if(!in_array($value, $response['user'], true)){
        array_push($response['user'], $value);
    }
}*/
 
    //echoRespnse(200, $response);
});
 
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Validating contact address
 */
function validatecontact($contact) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($contact, 1000)) {
        $response["error"] = true;
        $response["message"] = 'contact address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
$app->run();
?>