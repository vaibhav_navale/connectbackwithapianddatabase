<?php
 
error_reporting(-1);
ini_set('display_errors', 'On');
 
require_once '../include/db_handler.php';
require_once '../libs/gcm/gcm.php';
require_once '../libs/gcm/push.php';
require '.././libs/Slim/Slim.php';
 
Slim\Slim::registerAutoloader();
 
    $app = new Slim\Slim();
    $user_id = $app->request->post('user_id');
    $chatroom_id = $app->request->post('chatroom_id');
    
	$db = new DbHandler();
	$gcm = new GCM();
    $push = new Push();
	
	     $to_user = $db->getUser($user_id);
	     $from_user = $db->getUser($chatroom_id);
		 $data = array();
         $data['to_user'] = $to_user;
		 $data['from_user']	=$from_user;
		 $data['deliver']	="seen";	
		 
		  $push->setTitle("Connect");
          $push->setIsBackground(FALSE);
          $push->setFlag(PUSH_FLAG_USER);
          $push->setData($data); 
    
    $response = $db->deleteMessage($user_id, $chatroom_id);
    //echoRespnse(200, $response);
	 $gcm->send($to_user['gcm_registration_id'], $push->getPush());
      
    
    
    function echoRespnse($status_code, $response) {
        
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}

?>