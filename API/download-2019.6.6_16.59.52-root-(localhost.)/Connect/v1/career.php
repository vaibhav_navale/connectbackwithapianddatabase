<html lang="en">
<head>
<title>Career at PC Rounders - Software & Web Development Jobs</title>
<meta charset="UTF-8">
<meta name="description"
	content="Start your career at PC Rounders and flourish your future with one of the best offshore software development company in USA. Visit our website to know more about our job opening in software development and web development.">
<link rel="shortcut icon" type="image/x-icon"
	href="https://pcrounders.com/images/favicon.ico">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="https://pcrounders.com/career.html" rel="canonical" />
<link rel="stylesheet"
	href="vendor/bootstrap/css/bootstrap.custom.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/validate.js"></script>
<script src="js/main.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/full-slider.css" rel="stylesheet">
<script src="js/csi.js"></script>
<script src="js/jquery.js"></script>

<script>
	$(function() {
		$("#includedheader").load("header.html");
	});

	$(function() {
		$("#includedFooter").load("Footer.php");
	});
	$(function() {
		$("#includedSideButton").load("Sidebutton.html");
	});
</script>
  
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-131149190-1', 'auto');
ga('send', 'pageview');
</script>
  
</head>
<body>
	<div id="includedheader"></div>
	<header style="margin-top: 36px">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="images/banner1.png" alt="PC Rounders" title="PC Rounders"
						class="sliderimgstyle">
				</div>
				<div class="item">
					<img src="images/banner3.png" alt="PC Rounders" title="PC Rounders"
						class="sliderimgstyle">
				</div>
				<div class="item">
					<img src="images/banner2-1.png" alt="PC Rounders"
						title="PC Rounders" class="sliderimgstyle">
				</div>
			</div>
		</div>
	</header>
	<br>
	<br>


	<div class="container" style="padding: 20px;'">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center">Career In PC Rounders</h1>
			</div>
		</div>
		<br> <br>
		<div class="row">

			<div class="col-sm-12" style="padding-top: 20px;">

				<form method="post" action="career.php"
					enctype="multipart/form-data">

					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Name <font color="red">*</font></label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="name" id="name" placeholder="full name"
									onkeypress="return onlyAlphabets(event);" required /> <span
									id="name_s_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>



						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Email: <font color="red">*</font></label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="email" id="email" placeholder="email" required> <span
									id="email_s_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Mobile : <font color="red">*</font></label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="phone" id="phone" placeholder="mobile"
									onkeypress="return isNumber(event);" required> <span
									id="mobile_s_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>



						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Current Company :</label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="current_company" id="current_company"
									placeholder="current company"required /> <span
									id="current_company_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Linkedln URL :</label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="linkedln_url" id="linkedln_url"
									placeholder="linkedln url" required /> <span id="linkedln_url_error"
									class="text-danger font-weight-bold"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">GitHub URL :</label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="gitlab_url" id="gitlab_url" placeholder="gitlab url" required>
								<span id="gitlab_url_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>


					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Other Website :</label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="otherwebsite" id="otherwebsite"
									placeholder="otherwebsite url" required/> <span
									id="otherwebsite_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Portfolio URL :</label> <input
									style="border-radius: 20px;" type="text" class="form-control"
									name="portfolio_url" id="portfolio_url"
									placeholder="portfolio url" required> <span
									id="portfolio_url_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="usr">Resume : <font color="red">*</font></label> <input
									type="file" name="fileToUpload" id="photo" size="50"  accept="application/rtf,application/docx,application/doc,application/pdf,application/vnd.ms-excel" required /> <span
									id="photo_error" class="text-danger font-weight-bold"></span>
							</div>
						</div>

						<div class="col-sm-6">
						<!--	<button type="submit" class="btn btn-success subtab1 savebtn"
								style="color: white; width: 130px;" onclick="return Career();">
								save</button>-->
								
								<input type="submit" class="btn btn-success subtab1" value="Save"
							style="color:#000; width: 190px;" name="submit2">
						</div>

					</div>
				</form>
				<br> <br>
			</div>
		</div>
	</div>
	<div id="includedFooter"></div>
	<div id="includedSideButton"></div>
</body>
</html>


<?php

error_reporting(-1);
ini_set('display_errors', 'On');

if(isset($_REQUEST['submit2']))
{
    $connection = mysqli_connect("db756416182.db.1and1.com", "dbo756416182", "PCRounders1!","db756416182"); // Establishing Connection with Server
 // Selecting Database from Server
if(isset($_POST['submit2'])){ // Fetching variables of the form which travels in URL
$name = $_POST['name'];
$email = $_POST['email'];
$contact = $_POST['phone'];
$current_company = $_POST['current_company'];
$linkedln_url = $_POST['linkedln_url'];
$gitlab_url = $_POST['gitlab_url'];
$otherwebsite = $_POST['otherwebsite'];
$portfolio_url = $_POST['portfolio_url'];
$upload_path="";

$relative_path = '/upload/' . basename($_FILES['file']['name'][$i]);
$server_ip ="https://pcrounders.com/";
	  	
			$upload_url = $server_ip.'/'.$relative_path;	
				
		
			if (!move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_path)) {
			
				//File failed to be moved to the server , usually because the destination folder is not available
				$upload_path=$upload_url;
				
			}


//Insert Query of SQL
$query = mysqli_query($connection,"insert into  career (name,email,number,currentcompany,linkedln,gitlab,portfolio,otherweb,resume) values ('$name', '$email', '$contact', '$current_company','$linkedln_url','$gitlab_url','$portfolio_url','$otherwebsite','$upload_path')");
	// echo '<script>window.location.href = "index.html";</script>';


}
mysqli_close($connection);
}
