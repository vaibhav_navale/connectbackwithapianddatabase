<?php
 
error_reporting(-1);
ini_set('display_errors', 'On');
 
require_once '../include/db_handler.php';
require_once '../libs/gcm/gcm.php';
require_once '../libs/gcm/push.php';
require '.././libs/Slim/Slim.php';
 
Slim\Slim::registerAutoloader();
 
    $app = new Slim\Slim();
    
    $gcm_registration_id = $app->request->put('gcm_registration_id');
    $user_id = $app->request->put('user_id');
 
    $db = new DbHandler();
    $response = $db->updateGcmID($user_id, $gcm_registration_id);
 
    echoRespnse(200, $response);
    
    
    function echoRespnse($status_code, $response) {
        
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}

?>