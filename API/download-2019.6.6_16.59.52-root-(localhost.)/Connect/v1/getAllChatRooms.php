<?php
 
error_reporting(-1);
ini_set('display_errors', 'On');
 
require_once '../include/db_handler.php';
require_once '../libs/gcm/gcm.php';
require_once '../libs/gcm/push.php';
require '.././libs/Slim/Slim.php';
 
Slim\Slim::registerAutoloader();
 
    $app = new Slim\Slim();

   
    $db = new DbHandler();
 
    // fetching all user tasks
    $result = $db->getAllChatrooms();
 
    $response["error"] = false;
    $response["chat_rooms"] = array();
 
    // pushing single chat room into array
    while ($chat_room = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["chat_room_id"] = $chat_room["chat_room_id"];
        $tmp["name"] = $chat_room["name"];
        $tmp["created_at"] = $chat_room["created_at"];
        array_push($response["chat_rooms"], $tmp);
    }
 
    echoRespnse(200, $response);
	 
	 
function echoRespnse($status_code, $response) {
    
    
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
    
}




?>