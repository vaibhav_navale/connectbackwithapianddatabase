-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: con_app
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CarCompanyNameUSA`
--

DROP TABLE IF EXISTS `CarCompanyNameUSA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarCompanyNameUSA` (
  `usa_company_id` int(10) NOT NULL AUTO_INCREMENT,
  `usa_car_company_name` varchar(80) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`usa_company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CarCompanyNameUSA`
--

LOCK TABLES `CarCompanyNameUSA` WRITE;
/*!40000 ALTER TABLE `CarCompanyNameUSA` DISABLE KEYS */;
INSERT INTO `CarCompanyNameUSA` VALUES (1,'Acura'),(2,'Alfa Romeo'),(3,'Aston Martin'),(4,'Audi'),(5,'Bentley'),(6,'BMW'),(7,'Bugatti'),(8,'Buick'),(9,'Cadillac'),(10,'Chevrolet'),(11,'Chrysler'),(12,'Citroen'),(13,'Dodge'),(14,'Ferrari'),(15,'Fiat'),(16,'Ford'),(17,'Geely'),(18,'GMC'),(19,'Honda'),(20,'Hyundai'),(21,'Infiniti'),(22,'Jaguar'),(23,'Jeep'),(24,'Kia'),(25,'Koenigsegg'),(26,'Lamborghini'),(27,'Land Rover'),(28,'Lexus'),(29,'Maserati'),(30,'Mazda'),(31,'McLaren'),(32,'Mercedes Benz'),(33,'Mini'),(34,'Mitsubishi'),(35,'Nissan'),(36,'Pagani'),(37,'Peugeot'),(38,'Porsche'),(39,'Ram'),(40,'Renault'),(41,'Rolls Royce'),(42,'Saab'),(43,'Subaru'),(44,'Suzuki'),(45,'Tata Motors'),(46,'Tesla'),(47,'Toyota'),(48,'Volkswagen'),(49,'Volvo');
/*!40000 ALTER TABLE `CarCompanyNameUSA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CarNamesUSA`
--

DROP TABLE IF EXISTS `CarNamesUSA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarNamesUSA` (
  `usa_car_name_id` int(10) NOT NULL AUTO_INCREMENT,
  `usa_company_id` int(10) NOT NULL,
  `usa_car_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `usa_car_seating_capacity` int(10) NOT NULL,
  PRIMARY KEY (`usa_car_name_id`)
) ENGINE=MyISAM AUTO_INCREMENT=874 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CarNamesUSA`
--

LOCK TABLES `CarNamesUSA` WRITE;
/*!40000 ALTER TABLE `CarNamesUSA` DISABLE KEYS */;
INSERT INTO `CarNamesUSA` VALUES (1,1,'Acura CSX',4),(2,1,'Acura EL',4),(3,1,'Acura ILX',4),(4,1,'Acura MDX',4),(5,1,'Acura NSX',1),(6,1,'Acura RL',4),(7,1,'Acura RDX',4),(8,1,'Acura RLX',4),(9,1,'Acura RSX',3),(10,1,'Acura TL',4),(11,1,'Acura TLX',4),(12,1,'Acura TSX',4),(13,1,'Acura ZDX',4),(14,2,'Alfa Romeo 147',4),(15,2,'Alfa Romeo 156',4),(16,2,'Alfa Romeo 159',4),(17,2,'Alfa Romeo 166',4),(18,2,'Alfa Romeo 4C',1),(19,2,'Alfa Romeo 8C Competizione',1),(20,2,'Alfa Romeo Brera',3),(21,2,'Alfa Romeo Giulia',3),(22,2,'Alfa Romeo Giulietta',4),(23,2,'Alfa Romeo GT',3),(24,2,'Alfa Romeo GTV',3),(25,2,'Alfa Romeo MiTo',4),(26,2,'Alfa Romeo Spider',4),(27,2,'Alfa Romeo Stelvio',4),(28,3,'Aston Martin Cygnet',1),(29,3,'Aston Martin DB11',3),(30,3,'Aston Martin DB9',3),(31,3,'Aston Martin DBS',3),(32,3,'Aston Martin One 77',1),(33,3,'Aston Martin Rapide',3),(34,3,'Aston Martin V12 Vanquish',3),(35,3,'Aston Martin V12 Vantage',3),(36,3,'Aston Martin V8',2),(37,3,'Aston Martin Vanquish',3),(38,3,'Aston Martin Zagato',3),(39,4,'Audi A1',4),(40,4,'Audi A3',4),(41,4,'Audi A4',3),(42,4,'Audi A5',4),(43,4,'Audi A6',4),(44,4,'Audi A7',4),(45,4,'Audi A8',4),(46,4,'Audi  E tron',4),(47,4,'Audi PB18',1),(48,4,'Audi Q2',4),(49,4,'Audi Q3',4),(50,4,'Audi Q4 e tron',3),(51,4,'Audi Q5',4),(52,4,'Audi Q7',4),(53,4,'Audi Q8',5),(54,4,'Audi R8',1),(55,4,'Audi RS 3',4),(56,4,'Audi RS 4',4),(57,4,'Audi RS 5',4),(58,4,'Audi RS 6',4),(59,4,'Audi RS 7',3),(60,4,'Audi RS Q3',4),(61,4,'Audi S1',4),(62,4,'Audi S3',4),(63,4,'Audi S4',4),(64,4,'Audi S5',4),(65,4,'Audi S6',4),(66,4,'Audi S7',4),(67,4,'Audi S8',4),(68,4,'Audi SQ2',4),(69,4,'Audi SQ5',4),(70,4,'Audi SQ7',6),(71,4,'Audi TT',1),(72,5,'Bentley Continental GT',3),(73,5,'Bentley Azure',3),(74,5,'Bentley Continental GTC',3),(75,5,'Bentley Continental Flying Spur',3),(76,5,'Bentley Arnage',4),(77,5,'Bentley Continental',3),(78,5,'Bentley Brooklands',3),(79,5,'Bentley Continental Super',3),(80,5,'Bentley Azure T',3),(81,5,'Bentley Bentayga',4),(82,5,'Bentley Mulsanne',3),(83,6,'BMW 3 Series',4),(84,6,'BMW 5 Series',4),(85,6,'BMW 6 Series',4),(86,6,'BMW 7 Series',4),(87,6,'BMW 530',3),(88,6,'BMW M6',4),(89,6,'BMW M Roadster',4),(90,6,'BMW M5',4),(91,6,'BMW X3',4),(92,6,'BMW X5',6),(93,6,'BMW Alpina B7',4),(94,6,'BMW Z4 M',1),(95,6,'BMW 1 Series',3),(96,6,'BMW M3',4),(97,6,'BMW Z4',1),(98,6,'BMW X6',4),(99,6,'BMW Z4 M Roadster',1),(100,6,'BMW X5 M',4),(101,6,'BMW X6 M',4),(102,6,'BMW 7 Series',4),(103,6,'BMW X1',4),(104,6,'BMW X2',4),(105,6,'BMW X3 M',4),(106,6,'BMW i 3',3),(107,6,'BMW I 8',1),(108,6,'BMW X4',4),(109,6,'BMW X4 M',5),(110,6,'BMW X7',6),(111,6,'BMW M2',3),(112,6,'BMW M8',1),(113,7,'Bugatti Veyron',1),(114,7,'Bugatti Chiron',1),(115,7,'Bugatti Divo',1),(116,8,'Buick LaCrosse',4),(117,8,'Buick Rendezvous',6),(118,8,'Buick Terraza',6),(119,8,'Buick Rainier',4),(120,8,'Buick Lucerne',4),(121,8,'Buick Enclave',6),(122,8,'Buick Regal',4),(123,8,'Buick Verano',4),(124,8,'Buick Cascada',3),(125,8,'Buick Encore',4),(126,8,'Buick Envision',4),(127,8,'Buick Excelle',4),(128,8,'Buick GL6',5),(129,8,'Buick HRV Excelle',4),(130,9,'Cadillac CTS',4),(131,9,'Cadillac XLR V',1),(132,9,'Cadillac SRX',4),(133,9,'Cadillac Escalade',6),(134,9,'Cadillac XLR',2),(135,9,'Cadillac CTS V',4),(136,9,'Cadillac STS',4),(137,9,'Cadillac DTS',4),(138,9,'Cadillac Escalade ESV',6),(139,9,'Cadillac Escalade EXT',6),(140,9,'Cadillac STS V',4),(141,9,'Cadillac ATS',3),(142,9,'Cadillac BLS',4),(143,9,'Cadillac CT6',4),(144,9,'Cadillac ELR',4),(145,9,'Cadillac XT4',4),(146,9,'Cadillac XT5',4),(147,9,'Cadillac XT6',6),(148,9,'Cadillac XTS',5),(149,10,'Chevrolet Corvette',1),(150,10,'Chevrolet Colorado',4),(151,10,'Chevrolet Equinox',4),(152,10,'Chevrolet Silverado',4),(153,10,'Chevrolet Uplander',6),(154,10,'Chevrolet Cobalt SS',4),(155,10,'Chevrolet Lova',4),(156,10,'Chevrolet Cobalt',4),(157,10,'Chevrolet Tahoe',6),(158,10,'Chevrolet Suburban',7),(159,10,'Chevrolet HHR',4),(160,10,'Chevrolet Malibu',4),(161,10,'Chevrolet Monte Carlo',4),(162,10,'Chevrolet Aveo',4),(163,10,'Chevrolet Trailblazer',6),(164,10,'Chevrolet Express',15),(165,10,'Chevrolet Impala',4),(166,10,'Chevrolet Express 1500',11),(167,10,'Chevrolet Express 2500',5),(168,10,'Chevrolet Express 3500',15),(169,10,'Chevrolet Silverado 1500',5),(170,10,'Chevrolet Silverado 2500',6),(171,10,'Chevrolet Silverado 3500',6),(172,10,'Chevrolet Suburban 1500',6),(173,10,'Chevrolet Suburban 2500',6),(174,10,'Chevrolet Traverse',7),(175,10,'Chevrolet Camaro',3),(176,10,'Chevrolet Volt',4),(177,10,'Chevrolet Cruze',4),(178,10,'Chevrolet Sonic',4),(179,10,'Chevrolet Agile',4),(180,10,'Chevrolet Blazer',4),(181,10,'Chevrolet Bolt',4),(182,10,'Chevrolet Captiva',6),(183,10,'Chevrolet Cavalier',4),(184,10,'Chevrolet Epica',4),(185,10,'Chevrolet Metro',4),(186,10,'Chevrolet Orlando',6),(187,10,'Chevrolet Rezzo',4),(188,10,'Chevrolet Sail S RV',4),(189,10,'Chevrolet Spark',4),(190,10,'Chevrolet Trax',4),(191,11,'Chrysler Voyager',4),(192,11,'Chrysler Town and Country',6),(193,11,'Chrysler Sebring',4),(194,11,'Chrysler Aspen',5),(195,11,'Chrysler Crossfire',1),(196,11,'Chrysler Pacifica',6),(197,11,'Chrysler PT Cruiser',4),(198,11,'Chrysler 300',4),(199,11,'Chrysler 200',4),(200,12,'Citroen Berlingo',6),(201,12,'Citroen C1',3),(202,12,'Citroen C2',3),(203,12,'Citroen C3',4),(204,12,'Citroen C4',6),(205,12,'Citroen C5',4),(206,12,'Citroen C6',4),(207,12,'Citroen C8',6),(208,12,'Citroen e Mehari',3),(209,12,'Citroen Jumpy',8),(210,12,'Citroen SpaceTourer',8),(211,12,'Citroen C Zero',3),(212,12,'Citroen DS3',4),(213,12,'Citroen DS4',4),(214,12,'Citroen DS5',4),(215,12,'Citroen C Crosser',6),(216,12,'Citroen C Elysee',4),(217,13,'Dodge Ram',4),(218,13,'Dodge Charger',4),(219,13,'Dodge Magnum',4),(220,13,'Dodge Dakota',4),(221,13,'Dodge Caravan',6),(222,13,'Dodge Nitro',4),(223,13,'Dodge Caliber',4),(224,13,'Dodge Durango',6),(225,13,'Dodge Dakota Club',4),(226,13,'Dodge Grand Caravan',6),(227,13,'Dodge Ram 1500',5),(228,13,'Dodge Ram 2500',5),(229,13,'Dodge Ram 3500',5),(230,13,'Dodge Sprinter',4),(231,13,'Dodge Avenger',4),(232,13,'Dodge Viper',1),(233,13,'Dodge Challenger',4),(234,13,'Dodge Journey',6),(235,13,'Dodge Dart',4),(236,14,'Ferrari F430',1),(237,14,'Ferrari 612 Scaglietti',3),(238,14,'Ferrari California',1),(239,14,'Ferrari 488',1),(240,14,'Ferrari 599',1),(241,14,'Ferrari 812 Superfast',1),(242,14,'Ferrari Barchetta',1),(243,14,'Ferrari Enzo',1),(244,14,'Ferrari  F12',1),(245,14,'Ferrari  F8 Tributo',1),(246,14,'Ferrari Portofino',1),(247,15,'Fiat 500',4),(248,15,'Fiat Nuova 500',3),(249,15,'Fiat 124',1),(250,15,'Fiat Argo',4),(251,15,'Fiat Fullback',4),(252,15,'Fiat Linea',4),(253,15,'Fiat Bravo',4),(254,15,'Fiat Cronos',4),(255,15,'Fiat Doblo',6),(256,15,'Fiat Fiorino',4),(257,15,'Fiat Freemont',6),(258,15,'Fiat Talento',5),(259,15,'Fiat Stilo',4),(260,15,'Fiat Panda',4),(261,15,'Fiat Punto',4),(262,15,'Fiat Qubo',4),(263,15,'Fiat Sedici',4),(264,15,'Fiat Tipo',4),(265,15,'Fiat Strada',1),(266,15,'Fiat Toro',4),(267,16,'Ford Focus',4),(268,16,'Ford Escape',4),(269,16,'Ford Crown Victoria',5),(270,16,'Ford Fusion',4),(271,16,'Ford Mustang',2),(272,16,'Ford Fiesta',4),(273,16,'Ford Everest',6),(274,16,'Ford E Series',4),(275,16,'Ford Taurus',4),(276,16,'Ford Explorer Sport Trac',4),(277,16,'Ford Figo',4),(278,16,'Ford Flex',6),(279,16,'Ford Fusion',4),(280,16,'Ford GT500',3),(281,16,'Ford Galaxy',5),(282,16,'Ford GT',1),(283,16,'Ford KA',4),(284,16,'Ford Ranger',4),(285,16,'Ford EcoSport',4),(286,16,'Ford  B MAX',4),(287,16,'Ford F Series',3),(288,16,'Ford S MAX',6),(289,16,'Ford Shelby',3),(290,16,'Ford Sport Trac',4),(291,16,'Ford Territory',4),(292,16,'Ford Tourneo Courier',4),(293,16,'Ford Freestyle',4),(294,16,'Ford Five Hundred',3),(295,16,'Ford Explorer',6),(296,16,'Ford Edge',4),(297,16,'Ford Expedition',6),(298,16,'Ford E150',6),(299,16,'Ford E250',4),(300,16,'Ford E350',4),(301,16,'Ford F150',5),(302,16,'Ford F250',5),(303,16,'Ford F350',5),(304,16,'Ford Taurus X',4),(305,16,'Ford Taurus X',5),(306,16,'Ford F450',4),(307,16,'Ford Flex',6),(308,16,'Ford Transit Connect',5),(309,16,'Ford Fiesta',4),(310,17,'Geely Atlas',4),(311,17,'Geely Bo Rui GE',4),(312,17,'Geely Emgrand EV',4),(313,17,'Geely Emgrand GL',4),(314,17,'Geely Emgrand GS',4),(315,17,'Geely MK',4),(316,17,'Geely Otaka',4),(317,17,'Geely Emgrand X7',4),(318,17,'Geely Emgrand GT',4),(319,17,'Geely Vision',4),(320,17,'Geely FC',4),(321,18,'GMC Savana',7),(322,18,'GMC Acadia',6),(323,18,'GMC Envoy',4),(324,18,'GMC Sierra',4),(325,18,'GMC Yukon',8),(326,18,'GMC Canyon',3),(327,18,'GMC Savana 1500',7),(328,18,'GMC Savana 2500',3),(329,18,'GMC Savana 3500',7),(330,18,'GMC Sierra 1500',4),(331,18,'GMC Sierra 2500',5),(332,18,'GMC Sierra 3500',4),(333,18,'GMC Yukon XL 1500',8),(334,18,'GMC Yukon XL 2500',8),(335,18,'GMC Terrain',4),(336,19,'Honda Pilot',7),(337,19,'Honda Ridgeline',4),(338,19,'Honda Civic',4),(339,19,'Honda Odyssey',7),(340,19,'Honda S2000',1),(341,19,'Honda Fit',4),(342,19,'Honda Accord',4),(343,19,'Honda CR V',4),(344,19,'Honda Element',5),(345,19,'Honda Accord Crosstour',3),(346,19,'Honda CR Z',3),(347,19,'Honda FCX Clarity',4),(348,19,'Honda Insight',4),(349,19,'Honda Jade',5),(350,19,'Honda Jazz',4),(351,19,'Honda Legend',4),(352,19,'Honda N Box',3),(353,19,'Honda Crosstour',4),(354,19,'Honda Airwave',4),(355,19,'Honda Amaze',4),(356,19,'Honda Avancier',4),(357,19,'Honda BR V',6),(358,19,'Honda Brio',4),(359,19,'Honda City',4),(360,19,'Honda Clarity',4),(361,19,'Honda Civic',4),(362,19,'Honda Crossroad',6),(363,19,'Honda Element',4),(364,19,'Honda HR V',5),(365,19,'Honda N One',3),(366,19,'Honda NSX',1),(367,19,'Honda Odyssey',7),(368,19,'Honda Passport',4),(369,19,'Honda  UR V',4),(370,19,'Honda WR V',4),(371,19,'Honda Vezel',4),(372,19,'Honda VE 1',4),(373,20,'Hyundai Veracruz',6),(374,20,'Hyundai Accent',4),(375,20,'Hyundai Entourage',6),(376,20,'Hyundai Azera',4),(377,20,'Hyundai Sonata',4),(378,20,'Hyundai Tucson',4),(379,20,'Hyundai Elantra',4),(380,20,'Hyundai Santa Fe',6),(381,20,'Hyundai Veloster',3),(382,20,'Hyundai Genesis',4),(383,20,'Hyundai Genesis Coupe',3),(384,20,'Hyundai Equus',4),(385,20,'Hyundai HED 5',4),(386,20,'Hyundai ix55 Veracruz',4),(387,20,'Hyundai Kona',4),(388,20,'Hyundai Lafesta',4),(389,20,'Hyundai Lavita',4),(390,20,'Hyundai Nexo',4),(391,20,'Hyundai NF',4),(392,20,'Hyundai Palisade',7),(393,20,'Hyundai Santa FE',4),(394,20,'Hyundai Veloster',3),(395,20,'Hyundai Atos',4),(396,20,'Hyundai Celesta',4),(397,20,'Hyundai Centennial',4),(398,20,'Hyundai Coupe',4),(399,20,'Hyundai EON',4),(400,20,'Hyundai H 1',5),(401,20,'Hyundai i10',4),(402,20,'Hyundai i20',4),(403,20,'Hyundai i30',4),(404,20,'Hyundai i40',4),(405,20,'Hyundai IONIQ',4),(406,20,'Hyundai ix20',4),(407,20,'Hyundai ix25/Creta',4),(408,20,'Hyundai Santro',4),(409,20,'Hyundai Sonata',4),(410,20,'Hyundai Starex',7),(411,20,'Hyundai Trajet',6),(412,20,'Hyundai Verna',4),(413,20,'Hyundai Xcent',5),(414,20,'Hyundai  ix35',4),(415,21,'Infiniti Q50',5),(416,21,'Infiniti EX',4),(417,21,'Infiniti FX',4),(418,21,'Infiniti G37',3),(419,21,'Infiniti QX50',4),(420,21,'Infiniti M35',4),(421,21,'Infiniti QX56',7),(422,21,'Infiniti QX60',7),(423,21,'Infiniti QX70',4),(424,21,'Infiniti QX80',7),(425,21,'Infiniti Q60',3),(426,21,'Infiniti Project Black S',1),(427,21,'Infiniti  Q70',4),(428,21,'Infiniti QX30',4),(429,21,'Infiniti Q30',4),(430,22,'Jaguar XK',6),(431,22,'Jaguar X Type',4),(432,22,'Jaguar S Type',4),(433,22,'Jaguar XJ',4),(434,22,'Jaguar XKR',3),(435,22,'Jaguar XF',4),(436,22,'Jaguar E Pace',4),(437,22,'Jaguar F Pace',4),(438,22,'Jaguar I Pace',4),(439,22,'Jaguar XJR',4),(440,22,'Jaguar XK Series',3),(441,23,'Jeep Compass',4),(442,23,'Jeep Liberty',4),(443,23,'Jeep Patriot',4),(444,23,'Jeep Wrangle',3),(445,23,'Jeep Commander',6),(446,23,'Jeep Grand Cherokee',4),(447,23,'Jeep Cherokee',4),(448,23,'Jeep Gladiator',4),(449,23,'Jeep Grand Commander',6),(450,23,'Jeep Renegade',5),(451,24,'Kia Sedona',6),(452,24,'Kia Spectra',4),(453,24,'Kia Optima',4),(454,24,'Kia Sportage',4),(455,24,'Kia Carens',6),(456,24,'Kia Rio',4),(457,24,'Kia Sorento',6),(458,24,'Kia Amanti',4),(459,24,'Kia Rondo',6),(460,24,'Kia Rio5',4),(461,24,'Kia Mohave Borrego',4),(462,24,'Kia Stinger',4),(463,24,'Kia Stonic',4),(464,24,'Kia Telluride',7),(465,24,'Kia Venga',4),(466,24,'Kia Borrego',6),(467,24,'Kia Cadenza',4),(468,24,'Kia Carnival',7),(469,24,'Kia Cee d',5),(470,24,'Kia Cerato',4),(471,24,'Kia Forte',4),(472,24,'Kia K4',4),(473,24,'Kia KX3',4),(474,24,'Kia Magentis',4),(475,24,'Kia Mohave',4),(476,24,'Kia Niro',4),(477,24,'Kia Picanto',4),(478,24,'Kia Sorento',5),(479,24,'Kia Soul',4),(480,24,'Kia Pro Cee d',4),(481,25,'Koenigsegg Agera',1),(482,25,'Koenigsegg CC',1),(483,25,'Koenigsegg Jesko',1),(484,25,'Koenigsegg Regera',1),(485,25,'Koenigsegg One 1',1),(486,26,'Lamborghini Gallardo',1),(487,26,'Lamborghini Murciélago',1),(488,26,'Lamborghini Reventón',1),(489,26,'Lamborghini Murciélago LP640',1),(490,26,'Lamborghini Aventador',1),(491,26,'Lamborghini Centenario',1),(492,26,'Lamborghini Huracan',1),(493,26,'Lamborghini Urus',3),(494,27,'Land Rover Range Rover Sport',5),(495,27,'Land Rover Range Rover',4),(496,27,'Land Rover Discovery',5),(497,27,'Land Rover LR3',6),(498,27,'Land Rover Freelander',4),(499,27,'Land Rover LR2',4),(500,27,'Land Rover LR4',6),(501,27,'Land Rover Defender Ice Edition',9),(502,27,'Land Rover Defender',2),(503,27,'Land Rover Discovery Sport',5),(504,27,'Land Rover Range Rover Evoque',4),(505,27,'Land Rover Range Rover Velar',4),(506,28,'Lexus IS',4),(507,28,'Lexus SC',3),(508,28,'Lexus LX',6),(509,28,'Lexus RX',6),(510,28,'Lexus RX Hybrid',4),(511,28,'Lexus GX',5),(512,28,'Lexus GS',4),(513,28,'Lexus LS',4),(514,28,'Lexus ES',4),(515,28,'Lexus IS F',4),(516,28,'Lexus LS Hybrid',4),(517,28,'Lexus HS',4),(518,28,'Lexus CT',4),(519,28,'Lexus LFA',1),(520,28,'Lexus LC',3),(521,28,'Lexus NX',4),(522,28,'Lexus RC',3),(523,28,'Lexus UX',4),(524,29,'Maserati Quattroporte',4),(525,29,'Maserati Ghibli',4),(526,29,'Maserati Coupe',3),(527,29,'Maserati GranCabrio',3),(528,29,'Maserati Levante',4),(529,29,'Maserati Spyder',2),(530,29,'Maserati GranTurismo',3),(531,30,'Mazda B Series',4),(532,30,'Mazda Mazdaspeed6',4),(533,30,'Mazda Mazda6',4),(534,30,'Mazda Mazda',4),(535,30,'Mazda RX 8',3),(536,30,'Mazda CX 7',6),(537,30,'Mazda MX 5',1),(538,30,'Mazda Mazdaspeed 3',4),(539,30,'Mazda Mazda5',6),(540,30,'Mazda CX 9',6),(541,30,'Mazda Miata MX 5',1),(542,30,'Mazda Tribute',4),(543,30,'Mazda Mazda2',4),(544,30,'Mazda CX 5',4),(545,30,'Mazda Biante',7),(546,30,'Mazda CX 3',4),(547,30,'Mazda CX 4',4),(548,30,'Mazda CX 8',6),(549,30,'Mazda MPV',6),(550,31,'McLaren MP4 12C',1),(551,31,'McLaren 12C',1),(552,31,'McLaren 570S',1),(553,31,'McLaren 540C',1),(554,31,'McLaren 600LT',1),(555,31,'McLaren 650S',1),(556,31,'McLaren Senna',1),(557,31,'McLaren 675LT',1),(558,31,'McLaren 720S',1),(559,31,'McLaren P1',1),(560,32,'Mercedes Benz C Class',4),(561,32,'Mercedes Benz G Class',4),(562,32,'Mercedes Benz Viano',5),(563,32,'Mercedes Benz R Class',4),(564,32,'Mercedes Benz Vito',8),(565,32,'Mercedes Benz E Class',3),(566,32,'Mercedes Benz X class',4),(567,32,'Mercedes Benz CLS',4),(568,32,'Mercedes Benz GL',6),(569,32,'Mercedes Benz GLC',3),(570,32,'Mercedes Benz GLA',4),(571,32,'Mercedes Benz GLE',6),(572,32,'Mercedes Benz S Class',4),(573,32,'Mercedes Benz GLK',4),(574,32,'Mercedes Benz GLS',4),(575,32,'Mercedes Benz ML',4),(576,32,'Mercedes Benz SL',1),(577,32,'Mercedes Benz SLC',1),(578,32,'Mercedes Benz SLK',1),(579,32,'Mercedes Benz SLR McLaren',1),(580,32,'Mercedes Benz SLS AMG',1),(581,32,'Mercedes Benz  A class',4),(582,32,'Mercedes Benz AMG GT',4),(583,32,'Mercedes Benz B class',4),(584,32,'Mercedes Benz CL',4),(585,32,'Mercedes Benz CLA',4),(586,32,'Mercedes Benz CLC',3),(587,32,'Mercedes Benz CLK',3),(588,32,'Mercedes Benz V class',2),(589,33,'Mini Cooper',4),(590,33,'Mini Clubman',4),(591,33,'Mini Cooper Clubman',4),(592,33,'Mini Countryman',4),(593,33,'Mini Cooper Countryman',4),(594,33,'Mini Mini Roadster',1),(595,33,'Mini Convertible',3),(596,33,'Mini Countryman',4),(597,33,'Mini Coupe',3),(598,33,'Mini Hatch',3),(599,33,'Mini Paceman',3),(600,34,'Mitsubishi Eclipse',4),(601,34,'Mitsubishi Lancer',4),(602,34,'Mitsubishi  Space Star',4),(603,34,'Mitsubishi Galant',4),(604,34,'Mitsubishi Endeavor',6),(605,34,'Mitsubishi Outlander',6),(606,34,'Mitsubishi Lancer Evolution',4),(607,34,'Mitsubishi Tundra',5),(608,34,'Mitsubishi Outlander Sport',6),(609,34,'Mitsubishi i MiEV',3),(610,34,'Mitsubishi ASX',4),(611,34,'Mitsubishi Eclipse Cross',4),(612,34,'Mitsubishi L 200',4),(613,34,'Mitsubishi Mirage',4),(614,34,'Mitsubishi Pajero',6),(615,34,'Mitsubishi Xpander',6),(616,35,'Nissan Murano',4),(617,35,'Nissan Armada',7),(618,35,'Nissan Versa',4),(619,35,'Nissan Titan',5),(620,35,'Nissan 350Z',1),(621,35,'Nissan Frontier',4),(622,35,'Nissan Altima',4),(623,35,'Nissan Xterra',4),(624,35,'Nissan Maxima',4),(625,35,'Nissan Sentra',4),(626,35,'Nissan Quest',6),(627,35,'Nissan Pathfinder',6),(628,35,'Nissan Rogue',4),(629,35,'Nissan Cube',4),(630,35,'Nissan GT R',3),(631,35,'Nissan 370Z',1),(632,35,'Nissan Leaf',4),(633,35,'Nissan JUKE',4),(634,35,'Nissan NV1500',2),(635,35,'Nissan NV2500',2),(636,35,'Nissan NV3500',11),(637,35,'Nissan Bluebird',4),(638,35,'Nissan Almera',4),(639,35,'Nissan Note',4),(640,35,'Nissan Kicks',4),(641,35,'Nissan Micra',4),(642,35,'Nissan NP 300 Pick up',3),(643,35,'Nissan  NV200',1),(644,35,'Nissan Patrol',7),(645,35,'Nissan Pick UP',3),(646,35,'Nissan Qashqai',4),(647,35,'Nissan Pulsar',3),(648,35,'Nissan Rogue Sport',4),(649,35,'Nissan Sylphy',4),(650,35,'Nissan Teana',4),(651,35,'Nissan X Trail',6),(652,36,'Pagani Huayra',1),(653,36,'Pagani Zonda',1),(654,37,'Peugeot 207',3),(655,37,'Peugeot 107',3),(656,37,'Peugeot 108',4),(657,37,'Peugeot 2008',4),(658,37,'Peugeot 206',4),(659,37,'Peugeot 208',4),(660,37,'Peugeot 3008',4),(661,37,'Peugeot 301',4),(662,37,'Peugeot 308',4),(663,37,'Peugeot 307',4),(664,37,'Peugeot 4007',4),(665,37,'Peugeot 4008',4),(666,37,'Peugeot 408',4),(667,37,'Peugeot 5008',6),(668,37,'Peugeot 508',4),(669,37,'Peugeot 607',4),(670,37,'Peugeot 807',7),(671,37,'Peugeot Bipper',4),(672,37,'Peugeot  iOn',3),(673,37,'Peugeot Partner',4),(674,37,'Peugeot RCZ',3),(675,37,'Peugeot Rifter',4),(676,38,'Porsche 911',3),(677,38,'Porsche Boxster',1),(678,38,'Porsche Cayman',1),(679,38,'Porsche Mission E',3),(680,38,'Porsche Cayenne',4),(681,38,'Porsche Panamera',4),(682,38,'Porsche Macan',4),(683,38,'Porsche Carrera GT',1),(684,38,'Porsche 718',1),(685,38,'Porsche 918',1),(686,39,'Ram 1500',5),(687,39,'Ram 2500',5),(688,39,'Ram 3500',5),(689,39,'Ram Dakota',4),(690,39,'Ram C V',4),(691,40,'Renault Alaskan',4),(692,40,'Renault Captur',4),(693,40,'Renault Clio',4),(694,40,'Renault Duster',4),(695,40,'Renault Espace',6),(696,40,'Renault Fluence',4),(697,40,'Renault Kadjar',4),(698,40,'Renault Kangoo',1),(699,40,'Renault Koleos',4),(700,40,'Renault KWID',4),(701,40,'Renault Laguna',4),(702,40,'Renault Twingo',3),(703,40,'Renault Wind',1),(704,40,'Renault Latitude',4),(705,40,'Renault Lodgy',6),(706,40,'Renault Logan',4),(707,40,'Renault Megane',4),(708,40,'Renault Modus',4),(709,40,'Renault Pulse',4),(710,40,'Renault Sandero',4),(711,40,'Renault Scala',4),(712,40,'Renault Symbol',4),(713,40,'Renault Talisman',4),(714,40,'Renault Trafic',8),(715,40,'Renault Zoe',4),(716,41,'Rolls Royce Cullinan',4),(717,41,'Rolls Royce Dawn',3),(718,41,'Rolls Royce Ghost',4),(719,41,'Rolls Royce Phantom',3),(720,42,'Saab 9 2X',4),(721,42,'Saab 9 3',4),(722,43,'Subaru Legacy',4),(723,43,'Subaru Impreza',4),(724,43,'Subaru Outback',4),(725,43,'Subaru Forester',4),(726,43,'Subaru Tribeca',4),(727,43,'Subaru B9 Tribeca',4),(728,43,'Subaru Impreza WRX',4),(729,43,'Subaru BRZ',3),(730,43,'Subaru Ascent',7),(731,43,'Subaru BRZ',3),(732,43,'Subaru Justy',4),(733,43,'Subaru Levorg',4),(734,43,'Subaru XV',4),(735,43,'Subaru Trezia',4),(736,43,'Subaru WRX',4),(737,44,'Suzuki Daewoo Lacetti',4),(738,44,'Suzuki XL 7',6),(739,44,'Suzuki Reno',4),(740,44,'Suzuki SX4',4),(741,44,'Suzuki Aerio',4),(742,44,'Suzuki Grand Vitara',4),(743,44,'Suzuki Forenza',4),(744,44,'Suzuki XL7',6),(745,44,'Suzuki Equator',4),(746,44,'Suzuki Kizashi',4),(747,44,'Suzuki Alto',3),(748,44,'Suzuki Baleno',4),(749,44,'Suzuki Celerio',4),(750,44,'Suzuki Ciaz',4),(751,44,'Suzuki Dzire',4),(752,44,'Suzuki Ertiga',6),(753,44,'Suzuki Hustler',4),(754,44,'Suzuki Ignis',4),(755,44,'Suzuki Jimny',3),(756,44,'Suzuki Liana',4),(757,44,'Suzuki Splash',4),(758,44,'Suzuki Swift',4),(759,44,'Suzuki SX4',4),(760,44,'Suzuki Vitara',4),(761,44,'Suzuki Vitara Brezza',4),(762,45,'Tata Motors Bolt',4),(763,45,'Tata Motors Harrier',4),(764,45,'Tata Motors Hexa',6),(765,45,'Tata Motors Nano',3),(766,45,'Tata Motors Nexon',4),(767,45,'Tata Motors Safari',6),(768,45,'Tata Motors Tiago',4),(769,45,'Tata Motors Zest',4),(770,46,'Tesla Model 3',4),(771,46,'Tesla Model X',6),(772,46,'Tesla Tigor',4),(773,46,'Tesla Xenon',4),(774,46,'Tesla Model S',6),(775,47,'Toyota Matrix',4),(776,47,'Toyota Avalon',4),(777,47,'Toyota Hilux',3),(778,47,'Toyota Innova',7),(779,47,'Toyota iQ',4),(780,47,'Toyota Levin',4),(781,47,'Toyota Land Cruiser',4),(782,47,'Toyota Mirai',3),(783,47,'Toyota Passo',4),(784,47,'Toyota Yaris',4),(785,47,'Toyota Vios',4),(786,47,'Toyota Verso S',4),(787,47,'Toyota Verso',6),(788,47,'Toyota Venza',4),(789,47,'Toyota  Urban Cruiser',4),(790,47,'Toyota 4Runner',6),(791,47,'Toyota Camry',4),(792,47,'Toyota Proace',8),(793,47,'Toyota Prius',3),(794,47,'Toyota Sequoia',6),(795,47,'Toyota Progres',4),(796,47,'Toyota Sienna',7),(797,47,'Toyota Sienta',6),(798,47,'Toyota Supra',2),(799,47,'Toyota Tacoma',3),(800,47,'Toyota Alphard',6),(801,47,'Toyota Aurion',4),(802,47,'Toyota Auris',4),(803,47,'Toyota Avanza',7),(804,47,'Toyota Avensis',4),(805,47,'Toyota Aygo',3),(806,47,'Toyota Blade',4),(807,47,'Toyota  C HR',4),(808,47,'Toyota Caldina',4),(809,47,'Toyota Century',3),(810,47,'Toyota Corolla',4),(811,47,'Toyota Corolla Verso',6),(812,47,'Toyota Crown',4),(813,47,'Toyota Crown Majesta',4),(814,47,'Toyota Fortuner',6),(815,47,'Toyota GT 86',3),(816,47,'Toyota Harrier',4),(817,47,'Toyota Highlander',7),(818,48,'Volkswagen Touareg',5),(819,48,'Volkswagen Rabbit',4),(820,48,'Volkswagen Jetta',4),(821,48,'Volkswagen New Beetle',3),(822,48,'Volkswagen Eos',3),(823,48,'Volkswagen Passat',4),(824,48,'Volkswagen GLI',4),(825,48,'Volkswagen GTI',4),(826,48,'Volkswagen R32',4),(827,48,'Volkswagen Touareg 2',6),(828,48,'Volkswagen CC',4),(829,48,'Volkswagen Tiguan',4),(830,48,'Volkswagen Routan',6),(831,48,'Volkswagen Golf',4),(832,48,'Volkswagen T Roc',4),(833,48,'Volkswagen Sharan',5),(834,48,'Volkswagen T Cross',4),(835,48,'Volkswagen Phideon',4),(836,48,'Volkswagen Passat CC',3),(837,48,'Volkswagen Phaeton',4),(838,48,'Volkswagen Jetta',4),(839,48,'Volkswagen Amarok',4),(840,48,'Volkswagen Ameo',4),(841,48,'Volkswagen Arteon',4),(842,48,'Volkswagen Atlas',4),(843,48,'Volkswagen Bora',4),(844,48,'Volkswagen Caddy',4),(845,48,'Volkswagen Fox',4),(846,48,'Volkswagen Gol',4),(847,48,'Volkswagen I D',5),(848,48,'Volkswagen Golf',4),(849,48,'Volkswagen Up',3),(850,48,'Volkswagen Teramont',4),(851,48,'Volkswagen Tharu',4),(852,48,'Volkswagen Tayron',4),(853,48,'Volkswagen Polo',4),(854,48,'Volkswagen Scirocco',3),(855,48,'Volkswagen Multivan',7),(856,48,'Volkswagen Passat',4),(857,48,'Volkswagen Lavida',4),(858,48,'Volkswagen Lamando',4),(859,49,'Volvo V70',4),(860,49,'Volvo S40',4),(861,49,'Volvo S60',4),(862,49,'Volvo S80',4),(863,49,'Volvo XC70',4),(864,49,'Volvo C70',3),(865,49,'Volvo V50',4),(866,49,'Volvo XC90',5),(867,49,'Volvo C30',4),(868,49,'Volvo XC60',4),(869,49,'Volvo V60',4),(870,49,'Volvo V40',4),(871,49,'Volvo V90',4),(872,49,'Volvo S90',4),(873,49,'Volvo XC40',4);
/*!40000 ALTER TABLE `CarNamesUSA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_company`
--

DROP TABLE IF EXISTS `car_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_company` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(40) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`sr_no`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_company`
--

LOCK TABLES `car_company` WRITE;
/*!40000 ALTER TABLE `car_company` DISABLE KEYS */;
INSERT INTO `car_company` VALUES (1,'Tata Motors Limited'),(2,'Toyota'),(3,'Volkswagen India'),(4,'Volvo'),(5,'skoda india'),(6,'Porsche India'),(7,'Nissan Motor'),(8,'Mitsubishi Motors'),(9,'Mercedes-Benz India	'),(10,'Mahindra'),(11,'Maruti Suzuki India Limited'),(12,'Honda Cars India Ltd'),(13,'Hindustan Motors'),(14,'Ford'),(15,'Force Motors'),(16,'Fiat'),(17,'Chevrolet'),(18,'BMW'),(19,'Audi'),(20,'Hyundai Motor India Limited');
/*!40000 ALTER TABLE `car_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_names`
--

DROP TABLE IF EXISTS `car_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_names` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `car_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `seat` int(10) NOT NULL,
  PRIMARY KEY (`sr_no`)
) ENGINE=MyISAM AUTO_INCREMENT=237 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_names`
--

LOCK TABLES `car_names` WRITE;
/*!40000 ALTER TABLE `car_names` DISABLE KEYS */;
INSERT INTO `car_names` VALUES (1,20,'Eon',4),(2,20,'Santro',4),(3,20,'Grand i10 Prime',4),(4,20,'Grand i10',4),(5,20,'Elite i20',4),(6,20,'i20 Active',4),(7,20,'Xcent Prime',4),(8,20,'Xcent',4),(9,20,'Verna',4),(10,20,'Elantra',4),(11,20,'Creta',4),(12,20,'Tucson',4),(13,20,'Santa Fe',6),(14,1,'Tata Nexon',4),(15,1,'Tata Tiago',4),(16,1,'Tata Harrier',4),(17,1,'Tata Tigor',4),(18,1,'Tata Hexa',6),(19,1,'Tata Safari Storme',6),(20,1,'Tata Zest',4),(21,1,'Tata Sumo Gold',8),(22,1,'Tata Nano GenX',4),(23,1,'Tata Tiago NRG',4),(24,1,'Tata Bolt',4),(25,1,'Tata Tiago JTP',4),(26,1,'Tata Nano',4),(27,1,'Tata Tigor JTP',4),(28,1,'Tata Altroz',4),(29,2,'Toyota Fortuner',6),(30,2,'Toyota Innova Crysta',6),(31,2,'Toyota Platinum Etios',4),(32,2,'Toyota Corolla Altis',4),(33,2,'Toyota Yaris',4),(34,2,'Toyota Etios Liva',4),(35,2,'Toyota Camry 2019',4),(36,2,'Toyota Land Cruiser',6),(37,2,'Toyota Etios Cross',4),(38,2,'Toyota Land Cruiser Prado',6),(39,2,'Toyota Prius',4),(40,3,'Volkswagen Polo',4),(41,3,'Volkswagen Vento',4),(42,3,'Volkswagen Ameo',4),(43,3,'Volkswagen Tiguan',4),(44,3,'Volkswagen Passat',4),(45,3,'Volkswagen GTI',4),(46,3,'Volkswagen Generation Vento',4),(47,3,'Volkswagen New Polo',4),(48,3,'Volkswagen T-Cross',4),(49,4,'Volvo XC90',4),(50,4,'Volvo S60 Cross Country',4),(51,4,'Volvo S90',4),(52,4,'Volvo V40',4),(53,4,'Volvo V40 Cross Country',4),(54,4,'Volvo XC40',4),(55,4,'Volvo S60',4),(56,4,'Volvo V90 CROSS COUNTRY',4),(57,5,'Skoda Rapid',4),(58,5,'Skoda Octavia',4),(59,5,'Skoda Kodiaq',4),(60,5,'Skoda Superb',4),(61,5,'Skoda Karoq',4),(62,6,'Porsche Cayenne',5),(63,6,'Porsche 718',1),(64,6,'Porsche Macan',4),(65,6,'Porsche Panamera',4),(66,6,'Porsche 911',3),(67,6,'Porsche Macan 2019',4),(68,7,'Nissan Micra Active',4),(69,7,'Nissan Micra',4),(70,7,'Nissan Sunny',4),(71,7,'Nissan Kicks',4),(72,7,'Nissan Terrano',4),(73,7,'Nissan GTR',3),(74,7,'Nissan Qashqai',4),(75,7,'Nissan X-Trail Hybrid',4),(76,7,'Nissan Leaf',4),(77,7,'Nissan Patrol',6),(78,8,'Mitsubishi Pajero Sport',6),(79,8,'Mitsubishi Outlander',6),(80,8,'Mitsubishi Montero',6),(81,8,'Mitsubishi Mirage',4),(82,8,'Pajero Sport Facelift',6),(83,9,'Mercedes-Benz C-Class',3),(84,9,'Mercedes-Benz CLA',4),(85,9,'Mercedes-Benz A-Class',4),(86,9,'Mercedes-Benz E-Class',3),(87,9,'Mercedes-Benz S-Class',3),(88,9,'Mercedes-Benz GLA',4),(89,9,'Mercedes-Benz GLS',6),(90,9,'Mercedes-Benz GLE',4),(91,9,'Mercedes-Benz GLC',4),(92,9,'Mercedes-Benz G-Class',4),(93,9,'Mercedes-Benz CLS',4),(94,9,'Mercedes-Benz AMG GT',1),(95,9,'Mercedes-Benz B-Class',4),(96,9,'Mercedes-Benz V-Class',5),(97,9,'Mercedes-Benz SLC',1),(98,9,'Mercedes-Benz GLC Coupe',4),(99,9,'MercedesBenz C-Class Cabriolet',3),(100,9,'Mercedes-Benz GLE Coupe',4),(101,9,'Mercedes-Benz S-Coupe',3),(102,9,'Mercedes-Benz E-Class All Terrain',3),(103,9,'Mercedes-Benz S-Class Cabriolet',3),(104,9,'Mercedes-Benz GLE New',4),(105,10,'Mahindra XUV300',6),(106,10,'Mahindra Scorpio',6),(107,10,'Mahindra Bolero',6),(108,10,'Mahindra XUV500',6),(109,10,'Mahindra Marazzo',6),(110,10,'Mahindra KUV100 NXT',4),(111,10,'Mahindra Thar',5),(112,10,'Mahindra TUV300',6),(113,10,'Mahindra Alturas G4',6),(114,10,'Mahindra Verito',4),(115,10,'Mahindra Xylo',6),(116,10,'Mahindra NuvoSport',6),(117,10,'Mahindra TUV300 PLUS',8),(118,10,'Mahindra e2o PLUS',3),(119,10,'Mahindra Scorpio Getaway',4),(120,10,'Mahindra Verito Vibe CS',4),(121,10,'Mahindra e20 NXT',3),(122,10,'Mahindra eKUV100',4),(123,11,'Maruti Suzuki Baleno',4),(124,11,'Maruti Suzuki Vitara Brezza',4),(125,11,'Maruti Suzuki Swift',4),(126,11,'Maruti Suzuki Ertiga',4),(127,11,'Maruti Suzuki Wagon R',4),(128,11,'Maruti Suzuki Dzire',4),(129,11,'Maruti Suzuki Alto 800',4),(130,11,'Maruti Suzuki Celerio',4),(131,11,'Maruti Suzuki S-Cross',4),(132,11,'Maruti Suzuki Ignis',4),(133,11,'Maruti Suzuki Alto K10',4),(134,11,'Maruti Suzuki Ciaz',4),(135,11,'Maruti Suzuki Eeco',4),(136,11,'Maruti Suzuki Celerio X',4),(137,11,'Maruti Suzuki Omni',4),(138,11,'Maruti Suzuki Gypsy',4),(139,12,'Honda Amaze',4),(140,12,'Honda City',4),(141,12,'Honda Civic',4),(142,12,'Honda WR-V',4),(143,12,'Honda BR-V',6),(144,12,'Honda Jazz',4),(145,12,'Honda Brio',4),(146,12,'Honda CR-V',4),(147,12,'Honda Accord',4),(148,13,'Hindustan Motors Ambassador',4),(149,13,'Hindustan Motors Contessa',9),(150,14,'Ford Figo',4),(151,14,'Ford EcoSport',4),(152,14,'Ford Freestyle',4),(153,14,'Ford Aspire',4),(154,14,'Ford Mustang',3),(155,14,'Ford Endeavour',6),(156,14,'Ford Kuga',4),(157,15,'Force Motors Gurkha',6),(158,15,'Force One',6),(159,15,'Force Gurkha Xtreme',5),(160,16,'Fiat Punto EVO Pure',4),(161,16,'Fiat Punto Pure',4),(162,16,'Fiat Punto Evo',4),(163,16,'Fiat Linea Classic',4),(164,16,'Fiat Urban Cross',4),(165,16,'Fiat Avventura',4),(166,16,'Fiat Linea',4),(167,16,'Fiat Abarth Punto',4),(168,16,'Fiat Abarth Avventura',4),(169,16,'Fiat Abarth 595',3),(170,17,'Chevrolet Aveo',4),(171,17,'Chevrolet Aveo U VA',4),(172,17,'Chevrolet Beat',4),(173,17,'Chevrolet Captiva',6),(174,17,'Chevrolet Corvette',4),(175,17,'Chevrolet Cruze',4),(176,17,'Chevrolet Enjoy',7),(177,17,'Chevrolet Forester',7),(178,17,'Chevrolet Optra',4),(179,17,'Chevrolet Optra Magnum',4),(180,17,'Chevrolet Optra SRV',4),(181,17,'Chevrolet Sail',4),(182,17,'Chevrolet Sail Hatchback',4),(183,17,'Chevrolet Silverado',5),(184,17,'Chevrolet Spark',4),(185,17,' Chevrolet Tavera',9),(186,17,'Chevrolet Tavera Neo',9),(187,17,'Chevrolet Trailblazer',7),(188,18,'BMW X1',4),(189,18,'BMW 3 Series GT',4),(190,18,'BMW i8',3),(191,18,'BMW 7 Series',4),(192,18,'BMW X5',4),(193,18,'BMW X4',4),(194,18,'BMW 3 Series',4),(195,18,'BMW M2',3),(196,18,'BMW 6 Series',3),(197,18,'BMW X6',4),(198,18,'BMW 5 Series',4),(199,18,'BMW M Series',4),(200,18,'BMW X3',4),(201,18,'BMW 3 Series 2019',0),(202,18,'BMW 8 Series',3),(203,18,'BMW 7 Series 2019',4),(204,18,'BMW i3',3),(205,18,'BMW X5 2019',6),(206,18,'BMW X2',4),(207,18,'BMW X7',5),(208,18,'BMW 4 Series',4),(209,19,'Audi A3',4),(210,19,'Audi Q3',4),(211,19,'Audi A4',4),(212,19,'Audi A3 Cabriolet',3),(213,19,'Audi A6',4),(214,19,'Audi Q5',4),(215,19,'Audi A5',4),(216,19,'Audi TT',1),(217,19,'Audi A5 Cabriolet',3),(218,19,'Audi S5',4),(219,19,'Audi Q7',6),(220,19,'Audi RS 5',3),(221,19,'Audi A8L',3),(222,19,'Audi RS6',4),(223,19,'Audi RS7',4),(224,19,'Audi R8',1),(225,19,'Audi SQ7',5),(226,19,'Audi A1',3),(227,19,'Audi Q2',4),(228,19,'Audi Q3 Design Edition',4),(229,19,'Audi A4 Facelift 2019',6),(230,19,'Audi A6 Facelift',4),(231,19,'Audi A7 Facelift',4),(232,19,'Audi Q5 Design Edition',4),(233,19,'Audi TT Facelift',1),(234,19,'Audi Q8',4),(235,19,'Audi E-Tron',6),(236,19,'Audi A8 Facelift',3);
/*!40000 ALTER TABLE `car_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_rooms`
--

DROP TABLE IF EXISTS `chat_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_rooms` (
  `chat_room_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`chat_room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_rooms`
--

LOCK TABLES `chat_rooms` WRITE;
/*!40000 ALTER TABLE `chat_rooms` DISABLE KEYS */;
INSERT INTO `chat_rooms` VALUES (180,'Imran Shaikh',1559056044939),(187,'Krunal',1559227147685),(190,'Vaibhav Jio',1559379624166),(191,'Vaibhav Airtel',1559395614212);
/*!40000 ALTER TABLE `chat_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driver_data`
--

DROP TABLE IF EXISTS `driver_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `contact` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `city` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `profile_pic` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `licence` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `address_proof` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `id_proof` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `car_document` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `working_status` tinyint(1) NOT NULL,
  `otp` int(11) NOT NULL,
  `car_company_name` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `car_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `car_year` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `car_number` varchar(15) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver_data`
--

LOCK TABLES `driver_data` WRITE;
/*!40000 ALTER TABLE `driver_data` DISABLE KEYS */;
INSERT INTO `driver_data` VALUES (1,'test5','919579343254','Pune','1234567','http://connect.pcrounders.com//gcm_chat/v1/Connect/919579343254/Koala.jpg','http://connect.pcrounders.com//../Driver_Document//Lighthouse.jpg','http://connect.pcrounders.com//../Driver_Document//Desert.jpg','http://connect.pcrounders.com//../Driver_Document//Chrysanthemum.jpg','http://connect.pcrounders.com//../Driver_Document//Hydrangeas.jpg',0,0,'','','',''),(2,'test','919579343255','Pune','1234567','http://http://connect.pcrounders.com//gcm_chat/v1/Connect/9579343254/Desert.jpg','http://http://connect.pcrounders.com//Driver_Document/Hydrangeas.jpg','http://http://connect.pcrounders.com//Driver_Document/Jellyfish.jpg','http://http://connect.pcrounders.com//Driver_Document/Koala.jpg','http://http://connect.pcrounders.com//Driver_Document/Lighthouse.jpg',0,0,'','','',''),(18,'Imran','919579343035','pune','1234567','http://connect.pcrounders.com//gcm_chat/v1/Connect/919579343035/Connect_07032019_1534.PNG','http://connect.pcrounders.com//../Driver_Document//Screenshot_20190301-195254.png','http://connect.pcrounders.com//../Driver_Document//Screenshot_20190227-203408.png','http://connect.pcrounders.com//../Driver_Document//Screenshot_20190227-203337.png','http://connect.pcrounders.com//../Driver_Document//Screenshot_20190227-203352.png',1,0,'','','',''),(32,'Vaibhav Jio','918668511300','Pune','you','http://connect.pcrounders.com/v1/Connect/918668511300/Connect_27032019_1542.PNG','http://connect.pcrounders.com/Driver_Document/images (2).jpeg','http://connect.pcrounders.com/Driver_Document/images (1).jpeg','http://connect.pcrounders.com/Driver_Document/images.jpeg','http://connect.pcrounders.com/Driver_Document/download.jpeg',0,0,'Tata Motors Limited','Tata Safari Storme','2018','MH12kk9033'),(30,'Hh','18380923585','kk','yy','http://connect.pcrounders.com//gcm_chat/v1/Connect/18380923585/Connect_27032019_1300.PNG','http://connect.pcrounders.com//../Driver_Document//MCA 1st lot.pdf','http://connect.pcrounders.com//../Driver_Document//MCA 1st lot.pdf','http://connect.pcrounders.com//../Driver_Document//MCA 1st lot.pdf','http://connect.pcrounders.com//../Driver_Document//MCA 1st lot.pdf',0,0,'Rolls Royce','Rolls Royce Cullinan','2005','hj');
/*!40000 ALTER TABLE `driver_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `local_id` int(11) NOT NULL,
  `chat_room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `file` varchar(250) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `chat_room_id` (`chat_room_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_3` FOREIGN KEY (`chat_room_id`) REFERENCES `chat_rooms` (`chat_room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_relation`
--

DROP TABLE IF EXISTS `user_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_relation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `relation_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_relation`
--

LOCK TABLES `user_relation` WRITE;
/*!40000 ALTER TABLE `user_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_status`
--

DROP TABLE IF EXISTS `user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_status` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `contact` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `time` bigint(15) NOT NULL,
  PRIMARY KEY (`sr_no`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_status`
--

LOCK TABLES `user_status` WRITE;
/*!40000 ALTER TABLE `user_status` DISABLE KEYS */;
INSERT INTO `user_status` VALUES (41,180,'Imran Shaikh','919579343035',0,1559666066973),(49,190,'Vaibhav Jio','918668511300',0,1559748180235),(50,191,'Vaibhav Airtel','918380923585',0,1559748598403),(46,187,'Krunal','16095537266',0,1559666197966);
/*!40000 ALTER TABLE `user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `about` varchar(300) DEFAULT '',
  `gcm_registration_id` text,
  `created_at` bigint(20) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`contact`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (180,'919579343035','12345','http://8b8f6cb.online-server.cloud//Upload/UserProfiles/180/919579343035_180.PNG','Cool','e4wl2vCoIMw:APA91bEuYHbIgE21EYozzJI7WoNzm0eUij3tM3lLbWq6fAUNHmdhFGtf_T8qhQAGBXFxBAK2CU37X80rNKtxQR0PmuUE3fDU170_4B8mCD1lPYXC-7WmFd8b8c3uFwyf9JM_z4sNeU-p',1559056044939,NULL,'Imran Shaikh'),(187,'16095537266','12345','http://8b8f6cb.online-server.cloud//Upload/UserProfiles/187/16095537266_187_04062019_1213.PNG','Bored','fTg5Xpoc7HQ:APA91bG1kLnY3YJp3BrbtaPnvv_HEzzT7Yg1nJcLB56pSuYhsX9SpLblle_73-6x8-uqufVdSZTdKZpgaPYBFD82GtR7y8v6vAeM-gLpY-c8O41I-C5d5jINPwwQdUC_1joKqHHZq5yF',1559227147685,NULL,'Krunal'),(190,'918668511300','12345','http://8b8f6cb.online-server.cloud//Upload/UserProfiles/190/918668511300_190_05062019_1607.PNG','kio','cUpGbbVPZPg:APA91bEPei53ECbotXKouN1AIukDFImOUcCHfrG9yXxR4fNtjbwuO12-0cOQ9vPI37Mz-jCbAuaZaTLEqBl8eUXw3MTZ0tHl-wDgCZwufyEzcAPW3CCkhEgC2fPS4liRDoCv2wTaU0EG',1559379624166,NULL,'Vaibhav Jio'),(191,'918380923585','12345','http://8b8f6cb.online-server.cloud//Upload/UserProfiles/191/918380923585_191_05062019_1547.PNG','Some','eTQMqxVoLK8:APA91bHI0LwoNncc1cbj0bRmUTC3k1022rnOblw_4xXAlUWwOmZXkK13mENc7mnjmlC9nQc7MYzF7h7ekeIcLANXi954sdRcAlIjLQTTZa-Y-Ql4IzT-Jw8ZT1IA4Wne1CJ0TphZM0rr',1559395614212,NULL,'Vaibhav Airtel');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'con_app'
--

--
-- Dumping routines for database 'con_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-06 12:13:51
